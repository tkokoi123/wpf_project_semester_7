﻿using StoreManagement.Domain.Models;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class PaginationBarViewModel : BaseViewModel
    {
        #region Events
        #endregion

        #region Properties
        private int _totalPage;
        public int TotalPage { get => _totalPage; set { _totalPage = value; OnPropertyChanged(); } }

        private int _currentPage = 1;
        public int CurrentPage { get => _currentPage; set { _currentPage = value; OnPropertyChanged(); } }

        private int _previousPage;
        public int PreviousPage { get => _previousPage; set { _previousPage = value; OnPropertyChanged(); } }

        private int _nextPage;
        public int NextPage { get => _nextPage; set { _nextPage = value; OnPropertyChanged(); } }

        #endregion

        #region Command
        public ICommand NextpageCommand { get; set; }
        public ICommand PreviouspageCommand { get; set; }
        public ICommand GoEndPageCommand { get; set; }
        public ICommand GoFirstPageCommand { get; set; }

        #endregion


        public PaginationBarViewModel()
        {
            NextpageCommand = new RelayCommand<UserControl>(window => { return window == null ? false : true; }, p =>
            {
                FrameworkElement frameworkElement = GetParrentWindow(p);
                var currentWindow = (Window)frameworkElement;
                if (currentWindow != null)
                {
                    var dataContext = currentWindow.DataContext;
                    var dataContextName = dataContext.GetType().Name;
                    switch (dataContextName)
                    {
                        case "MainViewModel":
                            break;
                    }
                    
                }
            });

            PreviouspageCommand = new RelayCommand<Button>(window => { return window == null ? false : true; }, p =>
            {
                if (_currentPage > 1)
                {
                    _currentPage--;
                }
                else
                {
                    p.IsEnabled = false;
                }

            });

            GoEndPageCommand = new RelayCommand<Button>(window => { return window == null ? false : true; }, p =>
            {
                if (_currentPage < _totalPage)
                {
                    _currentPage = _totalPage;
                }
                else
                {
                    p.IsEnabled = false;
                }

            });

            GoFirstPageCommand = new RelayCommand<Button>(window => { return window == null ? false : true; }, p =>
            {
                if (_currentPage > 1)
                {
                    _currentPage = 1;
                }
                else
                {
                    p.IsEnabled = false;
                }

            });
        }

        private FrameworkElement GetParrentWindow(UserControl userControl)
        {
            FrameworkElement prarent = userControl;
            while (prarent.Parent != null)
            {
                prarent = (FrameworkElement)prarent.Parent;
            }
            return prarent;
        }
    }
}

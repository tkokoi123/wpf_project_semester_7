﻿using Domain.Models;
using Microsoft.Extensions.DependencyInjection;
using StoreManagement.Domain.Models;
using StoreManagement.Domain.RawModels;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Store;
using StoreManagement.Views;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private IServiceProvider _serviceProvider;
        private ObservableCollection<StmClient> _clientList;
        public ObservableCollection<StmClient> StmClients { get => _clientList; set { _clientList = value; OnPropertyChanged(); } }

        private ObservableCollection<StmSuplier> _suplierList;
        public ObservableCollection<StmSuplier> StmSupliers { get => _suplierList; set { _suplierList = value; OnPropertyChanged(); } }

        private ObservableCollection<StmCategory> _categoryList;
        public ObservableCollection<StmCategory> StmCategories { get => _categoryList; set { _categoryList = value; OnPropertyChanged(); } }

        private ObservableCollection<StmProduct> _productList;
        public ObservableCollection<StmProduct> StmProducts { get => _productList; set { _productList = value; OnPropertyChanged(); } }

        #region Properties
        private string _test;
        public string Test { get => _test; set { _test = value; OnPropertyChanged(); } }

        private string _searchClient;
        public string SearchClient { get => _searchClient; set { _searchClient = value; OnPropertyChanged(); } }

        private string _searchSuplier;
        public string SearchSuplier { get => _searchSuplier; set { _searchSuplier = value; OnPropertyChanged(); } }


        private string _searchProduct;
        public string SearchProduct { get => _searchProduct; set { _searchProduct = value; OnPropertyChanged(); } }
        
        
        #endregion

        #region Commands
        public ICommand WindowLoadedCommand { get; set; }

        public ICommand AddNewClientCommand { get; set; }
        public ICommand AddNewSuplierCommand { get; set; }

        public ICommand SearchClientCommand { get; set; }
        public ICommand SearchSuplierCommand { get; set; }
        public ICommand SearchProductCommand { get; set; }

        public ICommand ContactClientClickedCommand { get; set; }
        public ICommand ContactSuplierClickedCommand { get; set; }
        public ICommand ProductClickedCommand { get; set; }
        public ICommand IndexChange { get; set; }
        public ICommand OpenAttributeView { get; set; }
        public ICommand OpenCategoryView { get; set; }
        public ICommand OpenInputProductView { get; set; }
        public ICommand AddToCart { get; set; }
        public ICommand OpenSellProductView { get; set; }
        #endregion
        private readonly IClientService _clientService;

        public MainViewModel(IStoreManage storeManage, IClientService clientService, ISuplierService suplierService, IProductService productService, IServiceProvider serviceProvider, ICategoryService categoryService, AddClientView addClientView, AddSuplierView addSuplierView, AddProductView addProductView, UpdateProductView updateProductView)
        {
            SearchClient = "";
            SearchSuplier = "";
            _serviceProvider = serviceProvider;
            _clientService = clientService;
            WindowLoadedCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, async p =>
            {
                StmAccount stmAccount = (StmAccount)storeManage.Get("User");
                if (stmAccount != null)
                {
                    Test = $"Xin chào, {stmAccount.UserName}";
                }
                await LoadList().ConfigureAwait(false);
                StmSupliers = new ObservableCollection<StmSuplier>(suplierService.GetSupliers());
                StmCategories = new ObservableCollection<StmCategory>(categoryService.GetCategorys());
                StmProducts = new ObservableCollection<StmProduct>(productService.GetAllProduct());
            });

            #region Contact->Client
            AddNewClientCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                var dataContext = (AddClientViewModel)addClientView.DataContext;
                dataContext.Id = Guid.Empty;
                addClientView.ShowDialog();
            });

            SearchClientCommand = new RelayCommand<ListView>(lw => { return true; }, p =>
            {
                p.ItemsSource = clientService.SearchClient(SearchClient);
                //CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(p.ItemsSource);
                //view.Filter = UserFilter;
                CollectionViewSource.GetDefaultView(p.ItemsSource).Refresh();
            });

            ContactClientClickedCommand = new RelayCommand<ListView>(lw => { return lw == null ? false : true; }, p =>
            {
                if (p.SelectedIndex == -1)
                {
                    p.SelectedIndex = 0;
                }
                var s = (StmClient)p.SelectedItem;
                var dataContext = (AddClientViewModel)addClientView.DataContext;
                dataContext.Id = s.ClientId;
                dataContext.Phone = s.Phone;
                dataContext.Email = s.Email;
                dataContext.Address = s.Address;
                dataContext.ClientName = s.DisplayName;
                dataContext.MoreInfo = s.MoreInfo;
                addClientView.ShowDialog();
            });



            IndexChange = new RelayCommand<UserControl>(lw => { return lw == null ? false : true; }, p =>
            {
                MessageBox.Show(p.DataContext.GetType().ToString());
            });
            #endregion

            #region Contact -> Suplier
            AddNewSuplierCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                var dataContext = (AddSuplierViewModel)addSuplierView.DataContext;
                dataContext.Id = Guid.Empty;
                addSuplierView.ShowDialog();
            });

            

            SearchSuplierCommand = new RelayCommand<ListView>(lw => { return true; }, p =>
            {
                p.ItemsSource = suplierService.SearchSuplier(SearchSuplier);
                //CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(p.ItemsSource);
                //view.Filter = UserFilter;
                CollectionViewSource.GetDefaultView(p.ItemsSource).Refresh();
            });

            ContactSuplierClickedCommand = new RelayCommand<ListView>(lw => { return lw == null ? false : true; }, p =>
            {
                if (p.SelectedIndex == -1)
                {
                    p.SelectedIndex = 0;
                }
                var s = (StmSuplier)p.SelectedItem;
                var dataContext = (AddSuplierViewModel)addSuplierView.DataContext;
                dataContext.Id = s.SuplierId;
                dataContext.Phone = s.Phone;
                dataContext.Email = s.Email;
                dataContext.Address = s.Address;
                dataContext.SuplierName = s.DisplayName;
                dataContext.MoreInfo = s.MoreInfo;
                addProductView.ShowDialog();
            });

            SearchProductCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                var dataContext = (AddSuplierViewModel)addSuplierView.DataContext;
                dataContext.Id = Guid.Empty;
                addSuplierView.ShowDialog();
            });

            ProductClickedCommand = new RelayCommand<ListView>(lw => { return lw == null ? false : true; }, p =>
            {
                if (p.SelectedIndex == -1)
                {
                    p.SelectedIndex = 0;
                }
                var s = (StmProduct)p.SelectedItem;
                var dataContext = (UpdateProductViewModel)updateProductView.DataContext;
                dataContext.Product = s;

                updateProductView.ShowDialog();

            });

            AddToCart = new RelayCommand<StmProduct>(lw => { return lw == null ? false : true; }, p =>
            {
                StmOutputProduct ouputProduct = new StmOutputProduct(){ 
                    ProductId = p.ProductId,
                    EstimatedPrice = p.EstimatedPrice,
                    ProductName = p.ProductName
                };
                
                StmOutputProductManager.AddStmOutputProducts(ouputProduct);
            });


            #endregion
            OpenAttributeView = new RelayCommand<object>(p => { return true; }, p =>
            {
                var view = _serviceProvider.GetService<AttributeView>()!;
                view.ShowDialog();
            });
            OpenCategoryView = new RelayCommand<object>(p => { return true; }, p =>
            {
                var view = _serviceProvider.GetService<CategoryView>()!;
                view.ShowDialog();
            }); 
            OpenInputProductView = new RelayCommand<object>(p => { return true; }, p =>
            {
                var view = _serviceProvider.GetService<AddProductView>()!;
                view.ShowDialog();
            });
            OpenSellProductView = new RelayCommand<object>(p => { return true; }, p =>
            {
                var view = _serviceProvider.GetService<SellProductView>()!;
                view.ShowDialog();
            });
        }

        //private bool UserFilter(object item)
        //{
        //    if (string.IsNullOrEmpty(SearchClient))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return ((item as StmClient).DisplayName.
        //                    IndexOf(SearchClient, System.StringComparison.OrdinalIgnoreCase) >= 0);
        //    }
        //}

        private async Task LoadList()
        {
            StmClients = new ObservableCollection<StmClient>(_clientService.GetAll());

        }
    }
}

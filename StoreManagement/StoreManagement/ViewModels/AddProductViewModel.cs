﻿using Microsoft.Extensions.DependencyInjection;
using StoreManagement.Domain.Models;
using StoreManagement.Domain.RawModels;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class AddProductViewModel : BaseViewModel
    {
        private ICategoryService _categoryService;
        private IAttributeService _attributeService;
        private IProductService _productService;
        private IServiceProvider _serviceProvider;

        #region Commands
        public ICommand SelectedCategoryCommand { get; set; }
        public ICommand AddProductCommand { get; set; }
        public ICommand FinishCommand { get; set; }
        public ICommand ResetCommand { get; set; }
        public ICommand WindowLoaded { get; set; }
        public ICommand RemoveRawProductCommand { get; set; }

        #endregion

        #region Attribute
        //Category combobox
        private ObservableCollection<StmCategory> _StmCategorys;
        public ObservableCollection<StmCategory> StmCategorys { get => _StmCategorys; set { _StmCategorys = value; OnPropertyChanged(); } }
        //combobox selected
        private StmCategory _SelectedCategory;
        public StmCategory SelectedCategory { get => _SelectedCategory; set { _SelectedCategory = value; OnPropertyChanged(); } }

        //suplier combobox
        private ObservableCollection<StmSuplier> _StmSuplier;
        public ObservableCollection<StmSuplier> StmSuplier { get => _StmSuplier; set { _StmSuplier = value; OnPropertyChanged(); } }
        //suplier selected
        private StmSuplier _SelectedSuplier;
        public StmSuplier SelectedSuplier { get => _SelectedSuplier; set { _SelectedSuplier = value; OnPropertyChanged(); } }

        //listview Attribute by categoryId
        private ObservableCollection<RawStmAttribute> _RawStmAttributes;
        public ObservableCollection<RawStmAttribute> RawStmAttributes { get => _RawStmAttributes; set { _RawStmAttributes = value; OnPropertyChanged(); } }

        private ObservableCollection<RawStmProduct> _RawStmProducts;
        public ObservableCollection<RawStmProduct> RawStmProducts { get => _RawStmProducts; set { _RawStmProducts = value; OnPropertyChanged(); } }
        private string _ProductName;
        public string ProductName { get => _ProductName; set { _ProductName = value; OnPropertyChanged(); } }
        private string _ProductDescription;
        public string ProductDescription { get => _ProductDescription; set { _ProductDescription = value; OnPropertyChanged(); } }
        private int _Quantity;
        public int Quantity
        {
            get => _Quantity; set
            {
                _Quantity = value; OnPropertyChanged();
                if (Quantity == null)
                    Quantity = 1;
            }
        }
        private float _InputPrice;
        public float InputPrice { get => _InputPrice; set { _InputPrice = value; OnPropertyChanged(); } }
        private float _EstimatedPrice;
        public float EstimatedPrice { get => _EstimatedPrice; set { _EstimatedPrice = value; OnPropertyChanged(); } }

        #endregion
        public AddProductViewModel(IAttributeService attributeService,
            IProductService productService, ICategoryService categoryService,
            IServiceProvider serviceProvider)
        {
            _attributeService = attributeService;
            _productService = productService;
            _categoryService = categoryService;
            _serviceProvider = serviceProvider;
            StmCategorys = new ObservableCollection<StmCategory>(_categoryService.GetCategorys());
            //window loaded
            WindowLoaded = new RelayCommand<Window>(p => { return true; }, p =>
            {
                RawStmProducts = new ObservableCollection<RawStmProduct>();
            });
            //selectd category
            SelectedCategoryCommand = new RelayCommand<StmCategory>(p => { return true; }, p =>
            {
                var rawAttributes = _attributeService.GetAttributesByCategoryId(SelectedCategory.CategoryId).Select(a => new RawStmAttribute()
                {
                    AttributeId = a.AttributeId,
                    AttributeName = a.AttributeName,
                });
                RawStmAttributes = new ObservableCollection<RawStmAttribute>(rawAttributes);
            });

            AddProductCommand = new RelayCommand<object>(p => { return true; }, p =>
            {
                RawStmProducts.Add(new RawStmProduct()
                {
                    ProductName = ProductName,
                    ProductDescription = ProductDescription,
                    Quantity = Quantity,
                    InputPrice = InputPrice,
                    EstimatedPrice = EstimatedPrice,
                    Category = SelectedCategory,
                    RawStmAttributes = RawStmAttributes.ToList()
                });
                ResetValue();
            });

            RemoveRawProductCommand = new RelayCommand<RawStmProduct>(p => { return true; }, p =>
            {
                RawStmProducts.Remove(p);
            });
            FinishCommand = new RelayCommand<object>(p => { return true; }, p =>
            {
                if (RawStmProducts != null)
                {
                    var view = _serviceProvider.GetService<FinishAddProductView>()!;
                    var dataContext = (FinishAddProductViewModel)_serviceProvider.GetService<FinishAddProductView>()!.DataContext;
                    dataContext.RawStmProducts = RawStmProducts;
                    view.ShowDialog();
                    RawStmProducts.Clear();
                }
                else
                {
                    MessageBox.Show("chua co san pham nao");
                }
            });
        }
        public void ResetValue()
        {
            ProductName = String.Empty;
            ProductDescription = String.Empty;
            InputPrice = 0;
            Quantity = 1;
            RawStmAttributes = new ObservableCollection<RawStmAttribute>(RawStmAttributes.Select(a => new RawStmAttribute()
            {
                AttributeId = a.AttributeId,
                AttributeName = a.AttributeName
            }));
        }
    }
}

﻿using Domain.Models;
using Microsoft.Extensions.DependencyInjection;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Store;
using StoreManagement.Views;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    class LoginViewModel : BaseViewModel
    {
        #region Properties

        private string _userName;
        public string UserName { get => _userName; set { _userName = value; OnPropertyChanged(); } }

        private string _password;
        public string Password { get => _password; set { _password = value; OnPropertyChanged(); } }

        private string _errorMessage;
        public string ErrorMessage { get => _errorMessage; set { _errorMessage = value; OnPropertyChanged(); } }

        #endregion

        #region Commands
        public ICommand CloseWindowCommand { get; set; }
        public ICommand MouseMoveCommand { get; set; }
        public ICommand LoginCommand { get; set; }
        public ICommand PasswordChangeCommand { get; set; }
        #endregion
        public LoginViewModel(MainView mainView, IServiceProvider serviceProvider, IAccountService accountService, IStoreManage storeManage)
        {
            PasswordChangeCommand = new RelayCommand<PasswordBox>(p => { return p == null ? false : true; }, p =>
            {
                Password = p.Password;
            });

            LoginCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                var user = accountService.Login(UserName, Password);
                if (user != null)
                {
                    var storeUser = (StmAccount)storeManage.Get("User");
                    if (storeUser == null)
                    {
                        storeManage.Remove("User");
                        storeManage.Add("User", user);
                    }
                    p.Close();
                    mainView.Show();
                    serviceProvider.GetService<EmployeeView>()!.Close();
                }
                else
                {
                    ErrorMessage = "Sai tài khoản hoặc mật khẩu!";
                }
            });

            CloseWindowCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                p.Hide();
            });

            MouseMoveCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                p.DragMove();
            });
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class CategoryViewModel : BaseViewModel
    {
        private ICategoryService _categoryService;
        private IServiceProvider _serviceProvider;

        #region Commands
        public ICommand AddCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        public ICommand OpenWindowCategoryAttributeCommand { get; set; }
        #endregion

        #region Attribute
        private ObservableCollection<StmCategory> _StmCategorys;
        public ObservableCollection<StmCategory> StmCategorys { get => _StmCategorys; set { _StmCategorys = value; OnPropertyChanged(); } }
        private StmCategory _SelectedCategory;
        public StmCategory SelectedCategory
        {
            get => _SelectedCategory; set
            {
                _SelectedCategory = value; OnPropertyChanged(); if (SelectedCategory != null)
                {
                    CategoryName = SelectedCategory.CategoryName;
                    CategoryDescription = SelectedCategory.CategoryDescription;
                }
            }
        }

        private string _CategoryName;
        public string CategoryName { get => _CategoryName; set { _CategoryName = value; OnPropertyChanged(); } }

        private string _CategoryDescription;
        public string CategoryDescription { get => _CategoryDescription; set { _CategoryDescription = value; OnPropertyChanged(); } }
        private string _SearchValue;
        public string SearchValue { get => _SearchValue; set { _SearchValue = value; OnPropertyChanged(); } }
        
        #endregion
        public CategoryViewModel(ICategoryService categoryService,IServiceProvider serviceProvider)
        {

            _categoryService = categoryService;
            _serviceProvider = serviceProvider;

            StmCategorys = new ObservableCollection<StmCategory>(_categoryService.GetCategorys());

            AddCommand = new RelayCommand<object>(p => { return true; }, p =>
            {
                var result = _categoryService.AddCategory(new StmCategory()
                {
                    CategoryName = CategoryName,
                    CategoryDescription = CategoryDescription,
                }); ;
                if (!result)
                    MessageBox.Show("Thêm không thành công");
                //reload list Categorys
                StmCategorys = new ObservableCollection<StmCategory>(_categoryService.GetCategorys());
            });
            UpdateCommand = new RelayCommand<object>(p => { return true; }, p =>
            {

                //set new value for StmCategory
                SelectedCategory.CategoryName = CategoryName;
                SelectedCategory.CategoryDescription = CategoryDescription;

                var result = _categoryService.UpdateCategory(SelectedCategory);
                if (!result)
                    MessageBox.Show("Sửa không thành công!");
                //reload list Categorys
                StmCategorys = new ObservableCollection<StmCategory>(_categoryService.GetCategorys());
            });
            DeleteCommand = new RelayCommand<object>(p => { return true; }, p =>
            {
                var result = _categoryService.DisableCategory(SelectedCategory);
                if (!result)
                    MessageBox.Show("Xóa không thành công!");
                //reload list Categorys
                StmCategorys = new ObservableCollection<StmCategory>(_categoryService.GetCategorys());
            });
            SearchCommand = new RelayCommand<object>(p => { return true; }, p =>
            {
                StmCategorys = new ObservableCollection<StmCategory>(_categoryService.SearchCategoryByName(SearchValue));
            });
            //command open category attribute view
            OpenWindowCategoryAttributeCommand = new RelayCommand<object>(p => { return true; }, p =>
            {
                if (SelectedCategory != null)
                {
                    var view = _serviceProvider.GetService<CategoryAttributeView>()!;
                    var dataContext = (CategoryAttributeViewModel)_serviceProvider.GetService<CategoryAttributeView>()!.DataContext;
                    dataContext.CategoryId = SelectedCategory.CategoryId;
                    dataContext.SetValueForList();         
                    view.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Chưa chọn loại sản phẩm");
                }

            });
        }
    }
}

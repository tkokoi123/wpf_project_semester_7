﻿using StoreManagement.Views;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class EmployeeViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OpenLoginDialog { get; set; }
        #endregion

        #region Window
        private readonly LoginView _loginView;
        #endregion

        public EmployeeViewModel()
        {

        }

        public EmployeeViewModel(LoginView loginView)
        {
            _loginView = loginView;
            OpenLoginDialog = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                _loginView.ShowDialog();
            });
        }
    }
}

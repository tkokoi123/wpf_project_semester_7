﻿using Microsoft.Extensions.DependencyInjection;
using StoreManagement.Domain.Models;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Views;
using System;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class UpdateProductViewModel : BaseViewModel
    {
        private IServiceProvider _serviceProvider;
        private IProductService _productService;

        #region Commands
        public ICommand UpdateProductCommand { get; set; }
        public ICommand WindowLoaded { get; set; }

        #endregion

        #region Attribute
        private StmProduct _product;
        public StmProduct Product { get => _product; set { _product = value; OnPropertyChanged(); } }

        #endregion
        public UpdateProductViewModel(IProductService productService, IServiceProvider serviceProvider)
        {
            _productService = productService;
            _serviceProvider = serviceProvider;
            WindowLoaded = new RelayCommand<Window>(p => { return true; }, p =>
            {
                Product = productService.GetProductById(Product.ProductId);
            });
            UpdateProductCommand = new RelayCommand<Window>(p => { return true; }, p =>
            {
                var resultUpdate = _productService.UpdateProduct(Product);
                if (resultUpdate)
                {
                    MessageBox.Show("Cập nhật sản phẩm thành công!");
                    _serviceProvider.GetService<UpdateProductView>()!.Hide();
                }
                else
                {
                    MessageBox.Show("Cập nhật sản phẩm không thành công!");
                }
            });
        }

    }
}

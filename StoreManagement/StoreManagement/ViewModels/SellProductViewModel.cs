﻿using Microsoft.Extensions.DependencyInjection;
using StoreManagement.Domain.Models;
using StoreManagement.Domain.RawModels;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class SellProductViewModel : BaseViewModel
    {
        private IServiceTypeService _serviceTypeService;
        private IProductService _productService;
        private IClientService _clientService;
        private IServiceProvider _serviceProvider;

        #region Commands
        public ICommand RemoveStmOutputProductCommand { get; set; }
        public ICommand WindowLoaded { get; set; }
        public ICommand ConfirmCommand { get; set; }
        public ICommand SubmitCommand { get; set; }

        #endregion

        #region Attribute
        //List StmOutput 

        private ObservableCollection<StmOutputProduct> _StmOutputProducts;
        public ObservableCollection<StmOutputProduct> StmOutputProducts { get => _StmOutputProducts; set { _StmOutputProducts = value; OnPropertyChanged(); } }

        //product outbut (Bill)
        private StmProductOuput _ProductOutput;
        public StmProductOuput ProductOutput { get => _ProductOutput; set { _ProductOutput = value; OnPropertyChanged(); } }

        //service type
        private ObservableCollection<StmServiceType> _StmServiceTypes;
        public ObservableCollection<StmServiceType> StmServiceTypes { get => _StmServiceTypes; set { _StmServiceTypes = value; OnPropertyChanged(); } }
        private StmServiceType _SelectedServiceType;
        public StmServiceType SelectedServiceType { get => _SelectedServiceType; set { _SelectedServiceType = value; OnPropertyChanged(); } }
        //Client
        private ObservableCollection<StmClient> _StmClients;
        public ObservableCollection<StmClient> StmClients { get => _StmClients; set { _StmClients = value; OnPropertyChanged(); } }
        private StmClient _SelectedClient;
        public StmClient SelectedClient { get => _SelectedClient; set { _SelectedClient = value; OnPropertyChanged(); } }

        private float _MoneyForSupplies;
        public float MoneyForSupplies { get => _MoneyForSupplies; set { _MoneyForSupplies = value; OnPropertyChanged(); } }
        private string _TotalBill;
        public string TotalBill { get => _TotalBill; set { _TotalBill = value; OnPropertyChanged(); } }

        #endregion
        public SellProductViewModel(IProductService productService, IServiceProvider serviceProvider,
            IServiceTypeService serviceTypeService, IClientService clientService)
        {
            _productService = productService;
            _serviceTypeService = serviceTypeService;
            _clientService = clientService;
            _serviceProvider = serviceProvider;
            WindowLoaded = new RelayCommand<Window>(p => { return true; }, p =>
            {
                ProductOutput = new StmProductOuput();
                StmOutputProducts = StmOutputProductManager.GetStmOutputProducts();
                StmServiceTypes = new ObservableCollection<StmServiceType>(_serviceTypeService.GetServiceTypes());
                StmClients = new ObservableCollection<StmClient>(_clientService.GetAll());
                SetMoneyForSupplies();
            });
            RemoveStmOutputProductCommand = new RelayCommand<StmOutputProduct>(p => { return true; }, p =>
            {
                StmOutputProductManager.RemoveStmOutputProducts(p);
                StmOutputProducts = StmOutputProductManager.GetStmOutputProducts();
            });
            ConfirmCommand = new RelayCommand<Window>(p => { return true; }, p =>
            {
                SetMoneyForSupplies();
                SetTotalBill();
            });
            SubmitCommand = new RelayCommand<Window>(p => { return true; }, p =>
            {
                ProductOutput.ServiceTypeId = SelectedServiceType.TypeId;
                ProductOutput.ClientId = SelectedClient.ClientId;
                var resultAdd = _productService.OutputProduct(StmOutputProducts.ToList(), ProductOutput);
                if (resultAdd)
                {
                    MessageBox.Show("Bán thành công!");
                    StmOutputProductManager.RemoveAll();
                    var view = _serviceProvider.GetService<SellProductView>()!;
                    view.Hide();
                }
                else
                {
                    MessageBox.Show("Bán không thành công!");
                }
            });
        }
        public void SetMoneyForSupplies()
        {
            MoneyForSupplies = StmOutputProducts.Select(op => op.Quanity * op.EstimatedPrice).Sum();
        }
        public void SetTotalBill()
        {
            TotalBill = (MoneyForSupplies+ProductOutput.ServiceFee-ProductOutput.Discount).ToString()+"$";
        }
    }
}

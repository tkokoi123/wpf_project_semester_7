﻿using Microsoft.Extensions.DependencyInjection;
using StoreManagement.Domain.Models;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Views;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class AddSuplierViewModel : BaseViewModel
    {
        #region Properties
        private Guid _id;
        public Guid Id { get => _id; set { _id = value; OnPropertyChanged(); } }

        private string _suplierName;
        public string SuplierName { get => _suplierName; set { _suplierName = value; OnPropertyChanged(); } }

        private string _address;
        public string Address { get => _address; set { _address = value; OnPropertyChanged(); } }

        private string _phone;
        public string Phone { get => _phone; set { _phone = value; OnPropertyChanged(); } }

        private string _email;
        public string Email { get => _email; set { _email = value; OnPropertyChanged(); } }

        private string _moreinfo;
        public string MoreInfo { get => _moreinfo; set { _moreinfo = value; OnPropertyChanged(); } }
        #endregion

        #region Commands
        public ICommand WindowLoadedCommand { get; set; }
        public ICommand CloseDialogCommand { get; set; }
        public ICommand SaveChangeCommand { get; set; }
        #endregion

        public AddSuplierViewModel(ISuplierService suplierService, IServiceProvider serviceProvider)
        {
            CloseDialogCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                ClearTextBox();
                p.Hide();
            });

            SaveChangeCommand = new RelayCommand<Window>(window => { return window == null ? false : true; }, p =>
            {
                var suplier = new StmSuplier()
                {
                    SuplierId = Id,
                    DisplayName = SuplierName,
                    Address = Address,
                    Phone = Phone,
                    Email = Email,
                    MoreInfo = MoreInfo
                };

                if (Id == Guid.Empty)
                {
                    bool isSuccess = suplierService.AddSuplier(suplier);
                    if (isSuccess)
                    {
                        MessageBox.Show("Thêm khách hàng mới thành công!");
                        ClearTextBox();
                        var mainview = serviceProvider.GetService<MainView>();
                        var dataContext = (MainViewModel)mainview.DataContext;
                        dataContext.StmSupliers = new ObservableCollection<StmSuplier>(suplierService.GetSupliers());

                        p.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Có lỗi xảy ra khi thêm khách hàng!", "Lỗi!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {

                    bool isSuccess = suplierService.UpdateSuplier(suplier);
                    if (isSuccess)
                    {
                        MessageBox.Show("Cập nhật thông tin thành công!");
                        ClearTextBox();
                        var mainview = serviceProvider.GetService<MainView>();
                        var dataContext = (MainViewModel)mainview.DataContext;
                        dataContext.StmSupliers = new ObservableCollection<StmSuplier>(suplierService.GetSupliers());
                        p.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Có lỗi xảy ra khi cập nhật khách hàng!", "Lỗi!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }

            });
        }


        private void ClearTextBox()
        {
            SuplierName = string.Empty;
            Address = string.Empty;
            Phone = string.Empty;
            Email = string.Empty;
            MoreInfo = string.Empty;
        }
    }
}

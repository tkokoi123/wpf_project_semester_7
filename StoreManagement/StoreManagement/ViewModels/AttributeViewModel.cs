﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class AttributeViewModel : BaseViewModel
    {
        private IAttributeService _attributeService;
        #region Commands
        public ICommand AddCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        #endregion

        #region Attribute
        private ObservableCollection<StmAttribute> _StmAttributes;
        public ObservableCollection<StmAttribute> StmAttributes { get => _StmAttributes; set { _StmAttributes = value; OnPropertyChanged(); } }
        private StmAttribute _SelectedAttribute;
        public StmAttribute SelectedAttribute
        {
            get => _SelectedAttribute; set
            {
                _SelectedAttribute = value; OnPropertyChanged(); if (SelectedAttribute != null)
                {
                    AttributeName = SelectedAttribute.AttributeName;
                    AttributeDescription = SelectedAttribute.AttributeDescription;

                }
            }
        }

        private string _AttributeName;
        public string AttributeName { get => _AttributeName; set { _AttributeName = value; OnPropertyChanged(); } }

        private string _AttributeDescription;
        public string AttributeDescription { get => _AttributeDescription; set { _AttributeDescription = value; OnPropertyChanged(); } }
        private string _SearchValue;
        public string SearchValue { get => _SearchValue; set { _SearchValue = value; OnPropertyChanged(); } }

        #endregion
        public AttributeViewModel(IAttributeService attributeService)
        {
            _attributeService = attributeService;
StmAttributes = new ObservableCollection<StmAttribute>(_attributeService.GetAttributes());

            AddCommand = new RelayCommand<object>(p => { return true; }, p => {
                var result = _attributeService.AddAttribute(new StmAttribute()
                {
                    AttributeName = AttributeName,
                    AttributeDescription = AttributeDescription,
                });;
                if (!result)
                    MessageBox.Show("Thêm không thành công");
                //reload list attributes
                StmAttributes = new ObservableCollection<StmAttribute>(_attributeService.GetAttributes());
            });
            UpdateCommand = new RelayCommand<object>(p => { return true; }, p => {

                //set new value for StmAttribute
                SelectedAttribute.AttributeName = AttributeName;
                SelectedAttribute.AttributeDescription = AttributeDescription;

                var result = _attributeService.UpdateAttribute(SelectedAttribute);
                if (!result)
                    MessageBox.Show("Sửa không thành công!");
                //reload list attributes
                StmAttributes = new ObservableCollection<StmAttribute>(_attributeService.GetAttributes());
            });
            DeleteCommand = new RelayCommand<object>(p => { return true; }, p => {
                var result = _attributeService.DisableAttribute(SelectedAttribute);
                if (!result)
                    MessageBox.Show("Xóa không thành công!");
                //reload list attributes
                StmAttributes = new ObservableCollection<StmAttribute>(_attributeService.GetAttributes());
            });
            SearchCommand = new RelayCommand<object>(p => { return true; }, p => {
                StmAttributes = new ObservableCollection<StmAttribute>(_attributeService.SearchAttributeByName(SearchValue));
            });
        }
    }
}

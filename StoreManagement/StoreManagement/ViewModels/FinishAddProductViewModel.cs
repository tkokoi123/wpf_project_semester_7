﻿using Microsoft.Extensions.DependencyInjection;
using StoreManagement.Domain.Models;
using StoreManagement.Domain.RawModels;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class FinishAddProductViewModel : BaseViewModel
    {
        private ICategoryService _categoryService;
        private IAttributeService _attributeService;
        private IProductService _productService;
        private ISuplierService _suplierService;

        private IServiceProvider _serviceProvider;

        #region Commands
        public ICommand FinishCommand { get; set; }
        public ICommand WindowLoaded { get; set; }
        #endregion

        #region Attribute

        //suplier combobox
        private ObservableCollection<StmSuplier> _StmSuplier;
        public ObservableCollection<StmSuplier> StmSuplier { get => _StmSuplier; set { _StmSuplier = value; OnPropertyChanged(); } }
        //suplier selected
        private StmSuplier _SelectedSuplier;
        public StmSuplier SelectedSuplier { get => _SelectedSuplier; set { _SelectedSuplier = value; OnPropertyChanged(); } }


        private ObservableCollection<RawStmProduct> _RawStmProducts;
        public ObservableCollection<RawStmProduct> RawStmProducts { get => _RawStmProducts; set { _RawStmProducts = value; OnPropertyChanged(); } }
        
        private double _TotalMoney;
        public double TotalMoney { get => _TotalMoney; set { _TotalMoney = value; OnPropertyChanged(); } }


        private string _Description;
        public string Description { get => _Description; set { _Description = value; OnPropertyChanged(); } }
        private float _PreMoney;
        public float PreMoney { get => _PreMoney; set { _PreMoney = value; OnPropertyChanged(); } }


        #endregion
        public FinishAddProductViewModel(IAttributeService attributeService, IProductService productService,
            ICategoryService categoryService, ISuplierService suplierService, IServiceProvider serviceProvider)
        {
            _attributeService = attributeService;
            _productService = productService;
            _categoryService = categoryService;
            _suplierService = suplierService;
            _serviceProvider = serviceProvider;

            WindowLoaded = new RelayCommand<Window>(p => { return true; }, p =>
            {
                StmSuplier = new ObservableCollection<StmSuplier>(_suplierService.GetSupliers());
                TotalMoney = RawStmProducts.Select(p => p.InputPrice * p.Quantity).Sum();
            });
            FinishCommand = new RelayCommand<object>(p => { return true; }, p =>
           {
               var addResult = _productService.InputProduct(RawStmProducts.ToList(), SelectedSuplier, PreMoney, Description);
               if (addResult)
               {
                   MessageBox.Show("Nhập sản phẩm thành công!");
               }
               else
               {
                   MessageBox.Show("Nhập sản phẩm không thành công!");
               }
               _serviceProvider.GetService<FinishAddProductView>()!.Hide();
           });
        }

    }
}

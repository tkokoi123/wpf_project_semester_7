﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class CategoryAttributeViewModel : BaseViewModel
    {
        private IAttributeService _attributeService;
        private ICategoryAttributeService _categoryAttributeService;
        private IProductService _productService;
        #region Commands
        public ICommand SearchCategoryAttributeCommand { get; set; }
        public ICommand DeleteCategoryAttributeCommand { get; set; }

        public ICommand SearchAttributeCommand { get; set; }
        public ICommand AddCategoryAttributeCommand { get; set; }

        public ICommand WindowLoaded { get; set; }
        #endregion

        #region Attribute
        //CategoryId 
        private Guid _CategoryId;
        public Guid CategoryId { get => _CategoryId; set { _CategoryId = value; OnPropertyChanged(); } }

        //All attribute in system
        private ObservableCollection<StmAttribute> _StmAttributes;
        public ObservableCollection<StmAttribute> StmAttributes { get => _StmAttributes; set { _StmAttributes = value; OnPropertyChanged(); } }


        //Attribute name when used command SearchAttributeCommand
        private string _AttributeNameInAttribute;
        public string AttributeNameInAttribute { get => _AttributeNameInAttribute; set { _AttributeNameInAttribute = value; OnPropertyChanged(); } }


        //All Attribute of category id
        private ObservableCollection<StmAttribute> _StmCategoryAttributes;
        public ObservableCollection<StmAttribute> StmCategoryAttributes { get => _StmCategoryAttributes; set { _StmCategoryAttributes = value; OnPropertyChanged(); } }

        //Attribute name when used command SearchCategoryAttributeCommand
        private string _AttributeNameInCategory;
        public string AttributeNameInCategory { get => _AttributeNameInCategory; set { _AttributeNameInCategory = value; OnPropertyChanged(); } }
        #endregion

        public CategoryAttributeViewModel(IAttributeService attributeService, ICategoryAttributeService categoryAttributeService, IProductService productService)
        {
            _attributeService = attributeService;
            _categoryAttributeService = categoryAttributeService;
            _productService = productService;
            //Windowl loaded
            WindowLoaded = new RelayCommand<StmAttribute>(p => { return true; }, p => {
                SetValueForList();
            });
            //search attribute in category attribute
            SearchCategoryAttributeCommand = new RelayCommand<StmAttribute>(p => {
                if (AttributeNameInCategory == null)
                {
                    AttributeNameInCategory = String.Empty;
                }
                return true;
                ; }, p => {
                var stmCategoryAttributes = _attributeService.GetAttributesByCategoryIdAndAttributeName(CategoryId, AttributeNameInCategory);
                StmCategoryAttributes = new ObservableCollection<StmAttribute>(stmCategoryAttributes);
            });
            //remove attribute form category
            DeleteCategoryAttributeCommand = new RelayCommand<StmAttribute>(p => {
                //check category exist in product 
                //return _productService.CategoryExistInProduct(new Guid(CategoryId));
                return true;
            }, p => {
                var categoryAttribute = new StmCategoryAttribute()
                {
                    AttributeId = p.AttributeId,
                    CategoryId = CategoryId
                };
                _categoryAttributeService.DeleteCategoryAttribute(categoryAttribute);
                SetValueForList();
            });
            //search attribute in attribute not exist in category attribute
            SearchAttributeCommand = new RelayCommand<StmAttribute>(p => {
                if (AttributeNameInAttribute == null)
                {
                    AttributeNameInAttribute = String.Empty;
                }
                return true;
            }, p => {
                var listAttributeIds = _attributeService.GetAttributesByCategoryId(CategoryId).Select(ca => ca.AttributeId.ToString()).ToList();
                var stmAttributes = _attributeService.GetAttributesByAttributeNameAndNotContaintListAttributeId(AttributeNameInAttribute, listAttributeIds);
                StmAttributes = new ObservableCollection<StmAttribute>(stmAttributes);
            });
            //add attribute to category attribute
            AddCategoryAttributeCommand = new RelayCommand<StmAttribute>(p => { return true; }, p => {
                var stmCategoryAttribute = new StmCategoryAttribute()
                {
                    AttributeId = p.AttributeId,
                    CategoryId = CategoryId
                };
                _categoryAttributeService.AddCategoryAttribute(stmCategoryAttribute);
                SetValueForList();
            });

        }
        public void SetValueForList()
        {
            var stmCategoryAttributes = _attributeService.GetAttributesByCategoryId(CategoryId);
            StmCategoryAttributes = new ObservableCollection<StmAttribute>(stmCategoryAttributes);

            var listAttributeIds = stmCategoryAttributes.Select(ca => ca.AttributeId.ToString()).ToList();
            var stmAttributes = _attributeService.GetAttributesNotContaintListAttributeId(listAttributeIds);
            StmAttributes = new ObservableCollection<StmAttribute>(stmAttributes);
        }
    }
}

﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StoreManagement.ViewModels
{
    public class TitleBarViewModel : BaseViewModel
    {
        #region commands
        public ICommand CloseWindowCommand { get; set; }
        public ICommand MaximizeWindowCommand { get; set; }
        public ICommand MinimizeWindowCommand { get; set; }
        public ICommand MouseMoveCommand { get; set; }
        #endregion
        public TitleBarViewModel()
        {
            CloseWindowCommand = new RelayCommand<UserControl>(window => { return window == null ? false : true; }, p =>
            {
                FrameworkElement frameworkElement = GetParrentWindow(p);
                var currentWindow = (Window)frameworkElement;
                if (currentWindow != null)
                {
                    string currentWindowContext = currentWindow.DataContext.GetType().Name;
                    if (currentWindowContext.Equals(nameof(EmployeeViewModel)) || currentWindowContext.Equals(nameof(MainViewModel)))
                    {
                        Application.Current.Shutdown();
                    }
                    currentWindow.Hide();
                }
            });

            MaximizeWindowCommand = new RelayCommand<UserControl>(window => { return window == null ? false : true; }, p =>
            {
                FrameworkElement frameworkElement = GetParrentWindow(p);
                var currentWindow = (Window)frameworkElement;
                if (currentWindow != null)
                {
                    var windowState = currentWindow.WindowState;
                    if (windowState == WindowState.Maximized)
                    {
                        currentWindow.WindowState = WindowState.Normal;
                    }
                    else
                    {
                        currentWindow.WindowState = WindowState.Maximized;
                    }

                }
            });

            MinimizeWindowCommand = new RelayCommand<UserControl>(window => { return window == null ? false : true; }, p =>
            {
                FrameworkElement frameworkElement = GetParrentWindow(p);
                var currentWindow = (Window)frameworkElement;
                if (currentWindow != null)
                {
                    currentWindow.WindowState = WindowState.Minimized;
                }
            });

            MouseMoveCommand = new RelayCommand<UserControl>(window => { return window == null ? false : true; }, p =>
            {
                FrameworkElement frameworkElement = GetParrentWindow(p);
                var currentWindow = (Window)frameworkElement;
                if (currentWindow != null)
                {
                    currentWindow.DragMove();
                }
            });
        }

        private FrameworkElement GetParrentWindow(UserControl userControl)
        {
            FrameworkElement prarent = userControl;
            while (prarent.Parent != null)
            {
                prarent = (FrameworkElement)prarent.Parent;
            }
            return prarent;
        }
    }
}

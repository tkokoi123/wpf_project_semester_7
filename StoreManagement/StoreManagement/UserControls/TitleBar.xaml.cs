﻿using StoreManagement.ViewModels;
using System.Windows.Controls;

namespace StoreManagement.UserControls
{
    /// <summary>
    /// Interaction logic for TitleBar.xaml
    /// </summary>
    public partial class TitleBar : UserControl
    {
        public TitleBarViewModel ViewModel { get; set; }
        public TitleBar()
        {
            InitializeComponent();
            this.DataContext = ViewModel = new TitleBarViewModel();
        }
    }
}

﻿using StoreManagement.ViewModels;
using System.Windows.Controls;

namespace StoreManagement.UserControls
{
    /// <summary>
    /// Interaction logic for PaginationBar.xaml
    /// </summary>
    public partial class PaginationBar : UserControl
    {
        public PaginationBarViewModel ViewModel { get; set; }
        public PaginationBar()
        {
            InitializeComponent();
            this.DataContext = ViewModel = new PaginationBarViewModel();
        }
    }
}

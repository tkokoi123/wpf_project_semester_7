﻿using System.Collections.Generic;

namespace StoreManagement.Store
{
    public class StoreManage : IStoreManage
    {
        private Dictionary<string, object> store;

        public StoreManage()
        {
            store = new Dictionary<string, object>();
            if (!store.ContainsKey("User"))
            {
                store.Add("User", null);
            }
        }

        public void Add(string key, object value)
        {
            store[key] = value;
        }

        public object Get(string key)
        {
            return store[key];
        }

        public void Remove(string key)
        {
            store.Remove(key);
        }
    }
}

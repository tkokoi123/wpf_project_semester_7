﻿namespace StoreManagement.Store
{
    public interface IStoreManage
    {
        public void Add(string key, object value);
        public object Get(string key);
        public void Remove(string key);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StoreManagement.EF;
using StoreManagement.Infrastructure.Configurations;
using StoreManagement.Services.Services;
using StoreManagement.Services.Services.Interfaces;
using StoreManagement.Store;
using StoreManagement.ViewModels;
using StoreManagement.Views;
using System.Windows;

namespace StoreManagement.DependencyConfig
{
    public static class ServiceDependency
    {
        /// <summary>
        /// Register service with Di container
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddService(IServiceCollection services)
        {
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IAttributeService, AttributeService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICategoryAttributeService, CategoryAttributeService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ISuplierService, SuplierService>();
            services.AddScoped<IServiceTypeService, ServiceTypeService>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IStoreManage, StoreManage>();
            services.AddDbContext<AppDbContext>(option => option.UseNpgsql(AppSettings.Connection,
                                                    o => o.MigrationsAssembly("StoreManagement.EF")), ServiceLifetime.Singleton);
            return services;
        }

        public static IServiceCollection AddViewModel(IServiceCollection services)
        {
            services.AddSingleton<EmployeeViewModel>();
            services.AddSingleton<LoginViewModel>();
            services.AddSingleton<MainViewModel>();
            services.AddSingleton<AttributeViewModel>();
            services.AddSingleton<CategoryViewModel>();
            services.AddSingleton<CategoryAttributeViewModel>();
            services.AddSingleton<AddProductViewModel>();
            services.AddSingleton<FinishAddProductViewModel>();
            services.AddSingleton<UpdateProductViewModel>();
            services.AddSingleton<SellProductViewModel>();

            services.AddSingleton<AddClientViewModel>();
            services.AddSingleton<AddSuplierViewModel>();
            return services;
        }

        public static IServiceCollection AddWindow(IServiceCollection services)
        {
            services.AddSingleton<MainView>(s => new MainView()
            {
                DataContext = s.GetRequiredService<MainViewModel>()
            });

            services.AddSingleton<LoginView>(s => new LoginView()
            {
                DataContext = s.GetRequiredService<LoginViewModel>()
            });

            services.AddSingleton<EmployeeView>(s => new EmployeeView()
            {
                DataContext = s.GetRequiredService<EmployeeViewModel>()
            });

            services.AddSingleton<AttributeView>(s => new AttributeView()
            {
                DataContext = s.GetRequiredService<AttributeViewModel>()
            });

            services.AddSingleton<CategoryView>(s => new CategoryView()
            {
                DataContext = s.GetRequiredService<CategoryViewModel>()
            });

            services.AddSingleton<CategoryAttributeView>(s => new CategoryAttributeView()
            {
                DataContext = s.GetRequiredService<CategoryAttributeViewModel>()
            });
            services.AddSingleton<AddProductView>(s => new AddProductView()
            {
                DataContext = s.GetRequiredService<AddProductViewModel>()
            });
            services.AddSingleton<FinishAddProductView>(s => new FinishAddProductView()
            {
                DataContext = s.GetRequiredService<FinishAddProductViewModel>()
            });
            services.AddSingleton<UpdateProductView>(s => new UpdateProductView()
            {
                DataContext = s.GetRequiredService<UpdateProductViewModel>()
            });
            services.AddSingleton<SellProductView>(s => new SellProductView()
            {
                DataContext = s.GetRequiredService<SellProductViewModel>()
            });

            services.AddSingleton<AddClientView>(s => new AddClientView()
            {
                DataContext = s.GetRequiredService<AddClientViewModel>()
            });
            
            services.AddSingleton<AddSuplierView>(s => new AddSuplierView()
            {
                DataContext = s.GetRequiredService<AddSuplierViewModel>()
            }); 

            return services;
        }
    }
}

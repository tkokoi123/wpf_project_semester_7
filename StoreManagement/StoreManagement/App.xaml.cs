﻿using Microsoft.Extensions.DependencyInjection;
using StoreManagement.DependencyConfig;
using StoreManagement.Views;
using System;
using System.Windows;

namespace StoreManagement
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IServiceProvider _serviceProvider;

        public App()
        {
            IServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            _serviceProvider = services.BuildServiceProvider();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            ServiceDependency.AddViewModel(services);
            ServiceDependency.AddWindow(services);
            ServiceDependency.AddService(services);
        }

        private void OnStartup(object sender, StartupEventArgs e)
        {
            //var store = _serviceProvider.GetService<IStoreManage>()!;
            //if (store.Get("user") == null)
            //{
            var startupWindow = _serviceProvider.GetService<EmployeeView>()!;
            //var startupWindow = _serviceProvider.GetService<AttributeView>()!;
            //var startupWindow = _serviceProvider.GetService<CategoryView>()!;
            //var startupWindow = _serviceProvider.GetService<CategoryAttributeView>()!;
            //var startupWindow = _serviceProvider.GetService<AddProductView>()!;
            //var startupWindow = _serviceProvider.GetService<UpdateProductView>()!;

            //}
            //var mainWindow = serviceProvider.GetService<MainView>()!;
            startupWindow.Show();
        }

    }
}

﻿using StoreManagement.Domain.Models;
using StoreManagement.EF;
using StoreManagement.Services.Services.Interfaces;

namespace StoreManagement.Services.Services
{
    public class ClientService : IClientService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ClientService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<StmClient> GetAll()
        {
            return _unitOfWork.ClientRepository.GetAllClient().ToList();
        }

        public List<StmClient> SearchClient(string displayName)
        {
            return _unitOfWork.ClientRepository.SearchClient(displayName).ToList();
        }

        public List<StmClient> GetLimit(int index)
        {
            return _unitOfWork.ClientRepository.GetLimitClient(index).ToList();
        }

        public bool AddnewClient(StmClient stmClient)
        {
            try
            {
                if (stmClient.ClientId == Guid.Empty)
                {
                    _unitOfWork.ClientRepository.AddNewClient(stmClient);
                    _unitOfWork.Submit();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateClient(StmClient stmClient)
        {
            try
            {
                if (stmClient.ClientId != Guid.Empty)
                {
                    var client = _unitOfWork.ClientRepository.GetClientById(stmClient.ClientId);
                    client.Address = stmClient.Address;
                    client.Phone = stmClient.Phone;
                    client.DisplayName = stmClient.DisplayName;
                    client.Email = stmClient.Email;
                    client.MoreInfo = stmClient.MoreInfo;
                    _unitOfWork.Submit();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool InvalidClient(Guid id)
        {
            try
            {
                var client = _unitOfWork.ClientRepository.GetClientById(id);
                if (client != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

﻿using StoreManagement.Domain.Models;
using StoreManagement.EF;
using StoreManagement.Services.Services.Interfaces;

namespace StoreManagement.Services.Services
{
    public class AttributeService : IAttributeService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AttributeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public List<StmAttribute> GetAttributes()
        {
            try
            {
                return _unitOfWork.AttributeRepository.GetAttributes().ToList();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool AddAttribute(StmAttribute stmAttribute)
        {
            try {
                _unitOfWork.AttributeRepository.AddAttribute(stmAttribute);
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool UpdateAttribute(StmAttribute stmAttribute)
        {
            try
            {
                _unitOfWork.AttributeRepository.UpdateAttribute(stmAttribute);
                return true;
            }
            catch(Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool DisableAttribute(StmAttribute stmAttribute)
        {
            try
            {
                var attributeExist = _unitOfWork.CategoryAttributeRepository.AttributeExistInCategoryAttribute(stmAttribute);
                if (!attributeExist)
                {
                    _unitOfWork.AttributeRepository.DisableAttribute(stmAttribute);
                    return true;
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public StmAttribute GetAttributeById(string attributeId)
        {
            try
            {
                return _unitOfWork.AttributeRepository.GetAttributeById(attributeId);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public List<StmAttribute> SearchAttributeByName(string attributeName) {
            try
            {
                return _unitOfWork.AttributeRepository.SearchAttributeByName(attributeName).ToList();
            }
            catch (Exception ex) {
                return null;
            }
        }

        public List<StmAttribute> GetAttributesNotContaintListAttributeId(List<string> attributeIds)
        {
            try
            {
                return _unitOfWork.AttributeRepository.GetAttributesNotContaintListAttributeId(attributeIds).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<StmAttribute> GetAttributesByCategoryIdAndAttributeName(Guid categoryId, string attributeName)
        {
            try
            {
                return _unitOfWork.AttributeRepository.GetAttributesByCategoryIdAndAttributeName(categoryId, attributeName).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<StmAttribute> GetAttributesByAttributeNameAndNotContaintListAttributeId(string attributeName, List<string> attributeIds)
        {
            try
            {
                return _unitOfWork.AttributeRepository.GetAttributesByAttributeNameAndNotContaintListAttributeId(attributeName, attributeIds).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<StmAttribute> GetAttributesByCategoryId(Guid categoryId)
        {
            try
            {
                return _unitOfWork.AttributeRepository.GetAttributesByCategoryId(categoryId).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}

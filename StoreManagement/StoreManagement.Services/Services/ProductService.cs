﻿using StoreManagement.Domain.Models;
using StoreManagement.Domain.RawModels;
using StoreManagement.EF;
using StoreManagement.Services.Services.Interfaces;

namespace StoreManagement.Services.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public List<StmProduct> GetProductByCategory(Guid categoryId)
        {
            return _unitOfWork.ProductRepository.GetProductByCategory(categoryId).ToList();
        }
        public bool CategoryExistInProduct(Guid categoryId)
        {
            return _unitOfWork.ProductRepository.CategoryExistInProduct(categoryId);
        }
        public bool InputProduct(List<RawStmProduct> rawStmProducts, StmSuplier stmSuplier, float preMoney, string description)
        {
            try
            {
                //Add ProductInput
                var productInput = new StmProductInput()
                {
                    PreMoney = preMoney,
                    Description = description,
                    SuplierId = stmSuplier.SuplierId
                };
                _unitOfWork.ProductInputRepository.AddProductInput(productInput);
                foreach (RawStmProduct rawStmProduct in rawStmProducts)
                {
                    //add attribute value
                    var attributeValues = rawStmProduct.RawStmAttributes.Select(a => new StmAttributeValue()
                    {
                        AttributeId = a.AttributeId,
                        AttributeValue = a.AttributeValue
                    }).ToList();
                    _unitOfWork.AttributeValueRepository.AddAttributeValues(attributeValues);
                    //add product
                    var product = new StmProduct()
                    {
                        CategoryId = rawStmProduct.Category.CategoryId,
                        ProductName = rawStmProduct.ProductName,
                        ProductDescription = rawStmProduct.ProductDescription,
                        EstimatedPrice = rawStmProduct.EstimatedPrice,
                    };
                    _unitOfWork.ProductRepository.AddProduct(product);
                    _unitOfWork.Submit();
                    //product Attribute value
                    foreach (StmAttributeValue stmAttributeValue in attributeValues)
                    {
                        _unitOfWork.ProductAttributeValueRepository
                            .AddStmProductAttributeValue(new StmProductAttributeValue()
                            {
                                AttributeValueId = stmAttributeValue.AttributeValueId,
                                ProductId = product.ProductId
                            });
                    }
                    //productInputDetails
                    _unitOfWork.ProductInputDetailsRepository.AddProductInputDetails(new StmProductInputDetail()
                    {
                        ProdInputId = productInput.ProdInputId,
                        ProductId = product.ProductId,
                        InputPrice = rawStmProduct.InputPrice,
                        Quantity = rawStmProduct.Quantity
                    });
                    _unitOfWork.Submit();
                }
                return true;
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
                return false;
        }
        public StmProduct GetProductById(Guid productId)
        {
            try
            {
                return _unitOfWork.ProductRepository.GetProductById(productId);
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
                return null;
        }
        public bool UpdateProduct(StmProduct stmProduct)
        {
            try
            {
                _unitOfWork.ProductRepository.UpdateProduct(stmProduct);
                _unitOfWork.Submit();
            }catch(Exception ex)
            {
                return false;
            }
                return true;
        }
        public bool OutputProduct(List<StmOutputProduct> stmOutputProductsDetails, StmProductOuput  stmProductOutput)
        {
            try
            {
                //Add ProductOutput
               _unitOfWork.ProductOutputRepository.AddProductOutput(stmProductOutput);
                _unitOfWork.Submit();
                //Add ProductOutputDetails
                var productOutputDetails = stmOutputProductsDetails.Select(p => new StmProductOutputDetail()
                {
                    ProdOutputId= stmProductOutput.ProdOuputId,
                    ProductId = p.ProductId,
                    Quantity = p.Quanity,
                    OutputPrice = p.EstimatedPrice
                }).ToList();
                _unitOfWork.ProductOutputDetailsRepository.AddOutputProductDetails(productOutputDetails);
                _unitOfWork.Submit();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public IList<StmProduct> GetAllProduct()
        {
            return _unitOfWork.ProductRepository.GetAll().ToList();
        }
    }
}

﻿using StoreManagement.Domain.Models;

namespace StoreManagement.Services.Services.Interfaces
{
    public interface IClientService
    {
        public List<StmClient> GetAll();
        public List<StmClient> SearchClient(string displayName);
        public bool AddnewClient(StmClient stmClient);
        public bool UpdateClient(StmClient stmClient);
        public bool InvalidClient(Guid id);
    }
}

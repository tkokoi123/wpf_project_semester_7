﻿using StoreManagement.Domain.Models;

namespace StoreManagement.Services.Services.Interfaces
{
    public interface IServiceTypeService
    {
        public List<StmServiceType> GetServiceTypes();
    }
}

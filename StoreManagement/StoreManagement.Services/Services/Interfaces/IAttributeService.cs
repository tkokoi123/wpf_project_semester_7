﻿using StoreManagement.Domain.Models;

namespace StoreManagement.Services.Services.Interfaces
{
    public interface IAttributeService
    {
        public List<StmAttribute> GetAttributes();
        public bool AddAttribute(StmAttribute stmAttribute);
        public bool UpdateAttribute(StmAttribute stmAttribute);
        public bool DisableAttribute(StmAttribute stmAttribute);
        public StmAttribute GetAttributeById(string attributeId);
        public List<StmAttribute> SearchAttributeByName(string attributeName);
        public List<StmAttribute> GetAttributesByCategoryId(Guid categoryId);
        public List<StmAttribute> GetAttributesNotContaintListAttributeId(List<string> attributeIds);
        public List<StmAttribute> GetAttributesByAttributeNameAndNotContaintListAttributeId(string attributeName, List<string> attributeIds);
        public List<StmAttribute> GetAttributesByCategoryIdAndAttributeName(Guid categoryId, string attributeName);
    }
}

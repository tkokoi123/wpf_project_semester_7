﻿using StoreManagement.Domain.Models;

namespace StoreManagement.Services.Services.Interfaces
{
    public interface ISuplierService
    {
        public List<StmSuplier> GetSupliers();
        public bool AddSuplier(StmSuplier stmSuplier);
        public bool UpdateSuplier(StmSuplier stmSuplier);
        public List<StmSuplier> SearchSuplier(string displayName);
    }
}

﻿using StoreManagement.Domain.Models;
using StoreManagement.Domain.RawModels;

namespace StoreManagement.Services.Services.Interfaces
{
    public interface IProductService
    {
        public List<StmProduct> GetProductByCategory(Guid categoryId);
        public bool CategoryExistInProduct(Guid categoryId);
        public bool InputProduct(List<RawStmProduct> rawStmProducts, StmSuplier stmSuplier, float preMoney, string description);
        public StmProduct GetProductById(Guid productId);
        public bool UpdateProduct(StmProduct stmProduct);
        public bool OutputProduct(List<StmOutputProduct> stmOutputProductsDetails, StmProductOuput stmProductOutput);
        public IList<StmProduct> GetAllProduct();
    }
}

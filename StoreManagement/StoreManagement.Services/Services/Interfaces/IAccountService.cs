﻿using Domain.Models;

namespace StoreManagement.Services.Services.Interfaces
{
    public interface IAccountService
    {
        public StmAccount Login(string username, string password);
        public bool Register(string username, string password);
    }
}

﻿using StoreManagement.Domain.Models;

namespace StoreManagement.Services.Services.Interfaces
{
    public interface ICategoryAttributeService
    {
        public bool AddCategoryAttribute(StmCategoryAttribute stmCategoryAttribute);
        public bool DeleteCategoryAttribute(StmCategoryAttribute stmCategoryAttribute);
    }
}

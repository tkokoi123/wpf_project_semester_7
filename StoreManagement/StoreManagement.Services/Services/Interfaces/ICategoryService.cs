﻿using StoreManagement.Domain.Models;

namespace StoreManagement.Services.Services.Interfaces
{
    public interface ICategoryService
    {
        public List<StmCategory> GetCategorys();
        public bool AddCategory(StmCategory stmCategory);
        public bool UpdateCategory(StmCategory stmCategory);
        public bool DisableCategory(StmCategory stmCategory);
        public StmCategory GetCategoryById(string categoryId);
        public List<StmCategory> SearchCategoryByName(string categoryName);
    }
}

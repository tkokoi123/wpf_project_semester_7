﻿using StoreManagement.Domain.Models;
using StoreManagement.EF;
using StoreManagement.Services.Services.Interfaces;

namespace StoreManagement.Services.Services
{
    public class SuplierService : ISuplierService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SuplierService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<StmSuplier> GetSupliers()
        {
            return _unitOfWork.SuplierRepository.GetSupliers().ToList();
        }

        public bool AddSuplier(StmSuplier stmSuplier)
        {
            try
            {
                if (stmSuplier.SuplierId == Guid.Empty)
                {
                    _unitOfWork.SuplierRepository.AddSuplier(stmSuplier);
                    _unitOfWork.Submit();
                    return true;
                }
                return false;
            }catch(Exception ex)
            {
                return false;
            }
        }

        public bool UpdateSuplier(StmSuplier stmSuplier)
        {
            try
            {
                if (stmSuplier.SuplierId != Guid.Empty)
                {
                    var suplier = _unitOfWork.SuplierRepository.GetSuplierById(stmSuplier.SuplierId);
                    suplier.Address = stmSuplier.Address;
                    suplier.Phone = stmSuplier.Phone;
                    suplier.Email = stmSuplier.Email;
                    suplier.DisplayName = stmSuplier.DisplayName;
                    suplier.MoreInfo = stmSuplier.MoreInfo;
                    _unitOfWork.Submit();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<StmSuplier> SearchSuplier(string displayName)
        {
            return _unitOfWork.SuplierRepository.SearchSuplier(displayName).ToList();
        }
    }
}

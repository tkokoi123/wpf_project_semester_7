﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using StoreManagement.EF;
using StoreManagement.Infrastructure.Helpers;
using StoreManagement.Services.Services.Interfaces;

namespace StoreManagement.Services.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        public AccountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public StmAccount Login(string username, string password)
        {
            try
            {
                var user = _unitOfWork.AccountRepository
                                .GetAccountByUserName(username)
                                .FirstOrDefault();
                if (user != null)
                {
                    bool isValid = HashHelper.ComparePassword(user.Password, password);
                    if (isValid)
                    {
                        return user;
                    }
                }
            }
            catch
            {
                return null;
            }
            return null;
        }

        public bool Register(string username, string password)
        {
            try
            {
                StmAccount stmAccount = new StmAccount(username,
                                                        HashHelper.EncryptPassword(password));
                _unitOfWork
                    .AccountRepository
                    .AddNewAccount(stmAccount);

                _unitOfWork.Submit();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}

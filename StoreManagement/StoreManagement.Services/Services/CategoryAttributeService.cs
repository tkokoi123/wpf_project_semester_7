﻿using StoreManagement.Domain.Models;
using StoreManagement.EF;
using StoreManagement.Services.Services.Interfaces;

namespace StoreManagement.Services.Services
{
    public class CategoryAttributeService : ICategoryAttributeService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoryAttributeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        //Get IQueryable attribute by Category Id in entity stmCategoryAttribute
        
        public bool AddCategoryAttribute(StmCategoryAttribute stmCategoryAttribute) {
            try
            {
                _unitOfWork.CategoryAttributeRepository.AddCategoryAttribute(stmCategoryAttribute);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteCategoryAttribute(StmCategoryAttribute stmCategoryAttribute)
        {
            try
            {
                //check
                _unitOfWork.CategoryAttributeRepository.DeleteCategoryAttribute(stmCategoryAttribute);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

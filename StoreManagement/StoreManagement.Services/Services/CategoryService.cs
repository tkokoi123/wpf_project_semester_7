﻿using StoreManagement.Domain.Models;
using StoreManagement.EF;
using StoreManagement.Services.Services.Interfaces;

namespace StoreManagement.Services.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool AddCategory(StmCategory stmCategory)
        {
            try
            {
                _unitOfWork.CategoryRepository.AddCategory(stmCategory);
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            
        }

        public bool DisableCategory(StmCategory stmCategory)
        {
            try
            {
                _unitOfWork.CategoryRepository.DisableCategory(stmCategory);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public StmCategory GetCategoryById(string categoryId)
        {
            try
            {
                return _unitOfWork.CategoryRepository.GetCategoryById(categoryId); 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<StmCategory> GetCategorys()
        {
            try
            {
                return _unitOfWork.CategoryRepository.GetCategorys().ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<StmCategory> SearchCategoryByName(string categoryName)
        {
            try
            {
                return _unitOfWork.CategoryRepository.SearchCategoryByName(categoryName).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool UpdateCategory(StmCategory stmCategory)
        {
            try
            {
                _unitOfWork.CategoryRepository.UpdateCategory(stmCategory);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}

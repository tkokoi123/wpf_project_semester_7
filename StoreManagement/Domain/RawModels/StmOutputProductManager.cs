﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.Domain.RawModels
{
    public static class StmOutputProductManager
    {
        public static ObservableCollection<StmOutputProduct> stmOutputProducts = new ObservableCollection<StmOutputProduct>();
        public static void AddStmOutputProducts(StmOutputProduct stmOutputProduct)
        {
            stmOutputProducts.Add(stmOutputProduct);
        }
        public static void RemoveStmOutputProducts(StmOutputProduct stmOutputProduct)
        {
            stmOutputProducts.Remove(stmOutputProduct);
        }
        public static ObservableCollection<StmOutputProduct> GetStmOutputProducts()
        {
            return stmOutputProducts;
        }
        public static void RemoveAll()
        {
            stmOutputProducts.Clear();
        }
    }
}

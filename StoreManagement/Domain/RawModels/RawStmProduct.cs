﻿using StoreManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.Domain.RawModels
{
    public class RawStmProduct
    {
        public StmCategory Category { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public int Quantity { get; set; }
        public float InputPrice { get; set; }
        public float EstimatedPrice { get; set; }
        public List<RawStmAttribute> RawStmAttributes { get; set; }
    }
}

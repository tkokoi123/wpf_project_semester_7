﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    public class RawStmAttribute 
    {
        public Guid AttributeId { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }

    }
}

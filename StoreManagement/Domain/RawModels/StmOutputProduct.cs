﻿namespace StoreManagement.Domain.RawModels
{
    public class StmOutputProduct
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public float EstimatedPrice { get; set; }
        public int Quanity { get; set; } = 1;
        public int MaxQuanity { get; set; }
    }
}

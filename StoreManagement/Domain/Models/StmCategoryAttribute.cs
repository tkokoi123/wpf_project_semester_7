﻿using Domain.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_CategoryAttribute")]
    public class StmCategoryAttribute : BaseModel
    {
        public Guid CategoryAttributeId { get; set; }
        //[Key]
        public Guid CategoryId { get; set; }
        //[Key]
        public Guid AttributeId { get; set; }

        public virtual StmAttribute StmAttribute { get; set; }
        public virtual StmCategory StmCategory { get; set; }

        public StmCategoryAttribute(Guid categoryAttributeId, Guid categoryId, Guid attributeId)
        {
            CategoryAttributeId = categoryAttributeId;
            CategoryId = categoryId;
            AttributeId = attributeId;
        }

        public StmCategoryAttribute()
        {
        }
    }
}

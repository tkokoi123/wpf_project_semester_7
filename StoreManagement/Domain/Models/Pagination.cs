﻿namespace StoreManagement.Domain.Models
{
    public class Pagination
    {
        public int TotalPage { get; set; }
        public int CurrentPage { get; set; }
        public int PreviousPage { get; set; }
        public int Nextpage { get; set; }

        public Pagination(int totalPage, int currentPage, int previousPage, int nextpage)
        {
            TotalPage = totalPage;
            CurrentPage = currentPage;
            PreviousPage = previousPage;
            Nextpage = nextpage;
        }

        public Pagination()
        {
        }
    }
}

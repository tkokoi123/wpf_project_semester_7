﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("StmServiceType")]
    public class StmServiceType
    {
        [Key]
        public int TypeId { get; set; }
        [Required]
        public string TypeName { get; set; }

        public virtual ICollection<StmProductOuput> StmProductOuput { get; set; }

        public StmServiceType()
        {
            StmProductOuput = new HashSet<StmProductOuput>();
        }

        public StmServiceType(int typeId, string typeName)
        {
            TypeId = typeId;
            TypeName = typeName;
        }
    }
}

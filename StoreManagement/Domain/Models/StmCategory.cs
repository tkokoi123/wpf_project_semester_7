﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_Category")]
    public class StmCategory : BaseModel
    {
        [Key]
        public Guid CategoryId { get; set; }
        [Required]
        [MaxLength(500)]
        public string CategoryName { get; set; }

        public string? CategoryDescription { get; set; }

        public virtual ICollection<StmProduct> StmProducts { get; set; }
        public virtual ICollection<StmCategoryAttribute> StmCategoryAttributes { get; set; }

        public StmCategory()
        {
            StmProducts = new HashSet<StmProduct>();
            StmCategoryAttributes = new HashSet<StmCategoryAttribute>();
        }

        public StmCategory(Guid categoryId, string categoryName)
        {
            CategoryId = categoryId;
            CategoryName = categoryName;
        }
    }
}

﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_AttributeValue")]
    public class StmAttributeValue : BaseModel
    {
        [Key]
        public Guid AttributeValueId { get; set; }
        public Guid AttributeId { get; set; }
        [Required]
        [MaxLength(255)]
        public string AttributeValue { get; set; }
        public string? AttributeValueDescription { get; set; }

        public virtual StmAttribute StmAttribute { get; set; }
        public virtual ICollection<StmProductAttributeValue> StmProductAttributeValues { get; set; }

        public StmAttributeValue(Guid attributeValueId, Guid attributeId, string attributeValue, string attributeValueDescription)
        {
            AttributeValueId = attributeValueId;
            AttributeId = attributeId;
            AttributeValue = attributeValue;
            AttributeValueDescription = attributeValueDescription;
        }

        public StmAttributeValue()
        {
            StmProductAttributeValues = new HashSet<StmProductAttributeValue>();
        }
    }
}

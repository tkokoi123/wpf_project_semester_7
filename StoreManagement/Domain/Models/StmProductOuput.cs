﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_ProductOuput")]
    public class StmProductOuput : BaseModel
    {
        [Key]
        public Guid ProdOuputId { get; set; }
        [Required]
        public Guid ClientId { get; set; }
        public float ServiceFee { get; set; }
        public float Discount { get; set; }
        public float PrePayment { get; set; }
        public string Description { get; set; }
        public int ServiceTypeId { get; set; }

        public virtual ICollection<StmProductOutputDetail> StmProductOutputDetails { get; set; }
        public virtual StmServiceType StmServiceType { get; set; }
        public virtual StmClient StmClient { get; set; }

        public StmProductOuput(Guid prodOuputId, Guid clientId, float serviceFee, float discount, float prePayment, string description, int serviceTypeId)
        {
            ProdOuputId = prodOuputId;
            ClientId = clientId;
            ServiceFee = serviceFee;
            Discount = discount;
            PrePayment = prePayment;
            Description = description;
            ServiceTypeId = serviceTypeId;
        }

        public StmProductOuput()
        {
            StmProductOutputDetails = new HashSet<StmProductOutputDetail>();
        }
    }
}

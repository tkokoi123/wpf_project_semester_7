﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_ProductInput")]
    public class StmProductInput : BaseModel
    {
        [Key]
        public Guid ProdInputId { get; set; }
        [Required]
        public Guid SuplierId { get; set; }
        [Required]
        public float PreMoney { get; set; }
        public string Description { get; set; }

        public virtual ICollection<StmProductInputDetail> StmProductInputDetails { get; set; }
        public virtual StmSuplier StmSuplier { get; set; }


        public StmProductInput(Guid prodInputId)
        {
            ProdInputId = prodInputId;
        }

        public StmProductInput()
        {
            StmProductInputDetails = new HashSet<StmProductInputDetail>();
        }
    }
}

﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_Product")]
    public class StmProduct : BaseModel
    {
        [Key]
        public Guid ProductId { get; set; }
        public Guid CategoryId { get; set; }
        [Required]
        [MaxLength(1000)]
        public string ProductName { get; set; }
        public string? ProductDescription { get; set; }
        [Required]
        public float EstimatedPrice { get; set; }

        public virtual StmCategory CategoryIdNavigation { get; set; }
        public virtual ICollection<StmProductAttributeValue> StmProductAttributeValues { get; set; }
        public virtual ICollection<StmProductInputDetail> StmProductInputDetails { get; set; }
        public virtual ICollection<StmProductOutputDetail> StmProductOutputDetails { get; set; }

        public StmProduct()
        {
            StmProductAttributeValues = new HashSet<StmProductAttributeValue>();
            StmProductInputDetails = new HashSet<StmProductInputDetail>();
            StmProductOutputDetails = new HashSet<StmProductOutputDetail>();
        }

        public StmProduct(Guid productId, Guid categoryId, string productName, string productDescription)
        {
            ProductId = productId;
            CategoryId = categoryId;
            ProductName = productName;
            ProductDescription = productDescription;
        }
    }
}

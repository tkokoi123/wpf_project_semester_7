﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_ProductOutputDetails")]
    public class StmProductOutputDetail : BaseModel
    {
        [Key]
        public Guid ProdOutputDetailId { get; set; }

        [Required]
        public Guid ProdOutputId { get; set; }

        [Required]
        public Guid ProductId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public float OutputPrice { get; set; }

        public virtual StmProductOuput StmProductOuput { get; set; }
        public virtual StmProduct StmProduct { get; set; }

        public StmProductOutputDetail(Guid prodOutputDetailId, Guid prodOutputId, Guid productId, int quantity, float outputPrice)
        {
            ProdOutputDetailId = prodOutputDetailId;
            ProdOutputId = prodOutputId;
            ProductId = productId;
            Quantity = quantity;
            OutputPrice = outputPrice;
        }

        public StmProductOutputDetail()
        {
        }
    }
}

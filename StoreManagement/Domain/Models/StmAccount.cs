﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [Table("STM_Account")]
    public class StmAccount : BaseModel
    {
        [Key]
        [MaxLength(20)]
        public string UserName { get; set; }
        [Required]
        [MaxLength(255)]
        public string Password { get; set; }

        public StmAccount()
        {
        }

        public StmAccount(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }
    }
}

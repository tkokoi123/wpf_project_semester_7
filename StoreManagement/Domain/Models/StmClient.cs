﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_Client")]
    public class StmClient : BaseModel
    {
        [Key]
        public Guid ClientId { get; set; }
        [Required]
        public string DisplayName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [MaxLength(10)]
        public string Phone { get; set; }
        [MaxLength(255)]
        public string? Email { get; set; }
        public string? MoreInfo { get; set; }
        public bool IsVip { get; set; }

        public virtual ICollection<StmProductOuput> StmProductOutputs { get; set; }

        public StmClient()
        {
            StmProductOutputs = new HashSet<StmProductOuput>();
        }

        public StmClient(Guid clientId, string displayName, string address, string phone, string email, string moreInfo, bool isVip)
        {
            ClientId = clientId;
            DisplayName = displayName;
            Address = address;
            Phone = phone;
            Email = email;
            MoreInfo = moreInfo;
            IsVip = isVip;
        }
    }
}

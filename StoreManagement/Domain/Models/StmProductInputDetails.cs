﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_ProductInputDetails")]
    public class StmProductInputDetail : BaseModel
    {
        [Key]
        public Guid ProdInputDetailId { get; set; }

        [Required]
        public Guid ProdInputId { get; set; }

        [Required]
        public Guid ProductId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public float InputPrice { get; set; }

        public virtual StmProductInput StmProductInput { get; set; }
        public virtual StmProduct StmProduct { get; set; }
        public StmProductInputDetail(Guid prodInputDetailId, Guid prodInputId, Guid productId, int quantity, float inputPrice)
        {
            ProdInputDetailId = prodInputDetailId;
            ProdInputId = prodInputId;
            ProductId = productId;
            Quantity = quantity;
            InputPrice = inputPrice;
        }

        public StmProductInputDetail()
        {
        }
    }
}

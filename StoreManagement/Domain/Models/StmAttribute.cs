﻿using Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_Attribute")]
    public class StmAttribute : BaseModel
    {
        [Key]
        public Guid AttributeId { get; set; }
        [Required]
        [MaxLength(300)]
        public string AttributeName { get; set; }

        public string? AttributeDescription { get; set; }

        public virtual ICollection<StmCategoryAttribute> StmCategoryAttributes { get; set; }
        public virtual ICollection<StmAttributeValue> StmAttributeValues { get; set; }

        public StmAttribute(Guid attributeId, string attributeName, string? attributeDescription)
        {
            AttributeId = attributeId;
            AttributeName = attributeName;
            AttributeDescription = attributeDescription;
        }

        public StmAttribute()
        {
            StmCategoryAttributes = new HashSet<StmCategoryAttribute>();
            StmAttributeValues = new HashSet<StmAttributeValue>();
        }
    }
}

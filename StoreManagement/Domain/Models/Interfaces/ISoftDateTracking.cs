﻿namespace Domain.Models.Interfaces
{
    internal interface ISoftDateTracking
    {
        public bool IsDelete { get; set; }
    }
}

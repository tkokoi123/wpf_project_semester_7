﻿using Domain.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreManagement.Domain.Models
{
    [Table("STM_ProductAttributeValue")]
    public class StmProductAttributeValue : BaseModel
    {
        //Key
        public Guid ProductId { get; set; }
        //Key
        public Guid AttributeValueId { get; set; }

        public virtual StmProduct StmProductNav { get; set; }
        public virtual StmAttributeValue StmAttributeValueNav { get; set; }

        public StmProductAttributeValue(Guid productId, Guid attributeValueId)
        {
            ProductId = productId;
            AttributeValueId = attributeValueId;
        }

        public StmProductAttributeValue()
        {
        }
    }
}

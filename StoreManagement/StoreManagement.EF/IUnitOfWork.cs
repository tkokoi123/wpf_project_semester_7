﻿using StoreManagement.EF.Repositories.Interface;

namespace StoreManagement.EF
{
    public interface IUnitOfWork
    {
        public IStmClientRepository ClientRepository { get; }
        public IStmAttributeRepository AttributeRepository { get; }
        public IStmCategoryAttributeRepository CategoryAttributeRepository { get; }
        public IStmCategoryRepository CategoryRepository { get; }
        public IStmAccountRepository AccountRepository { get; }
        public IStmProductRepository ProductRepository { get; }
        public IStmSuplierRepository SuplierRepository { get; }
        public IStmAttributeValueRepository AttributeValueRepository { get; }
        public IStmProductAttributeValueRepository ProductAttributeValueRepository { get; }
        public IStmProductInputRepository ProductInputRepository { get; }
        public IStmProductInputDetailsRepository ProductInputDetailsRepository { get; }
        public IStmServiceTypeRepository ServiceTypeRepository { get; }
        public IStmProductOutputRepository ProductOutputRepository { get; }
        public IStmProductOutputDetailsRepository ProductOutputDetailsRepository { get; }

        public Task SubmitAsync();
        public void Submit();
    }
}

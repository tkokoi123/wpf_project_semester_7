﻿using Domain.Models;
using Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using StoreManagement.Domain.Models;

namespace StoreManagement.EF
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        #region DbSet
        public DbSet<StmAccount> StmAccounts { get; set; }
        public DbSet<StmClient> StmClients { get; set; }
        public DbSet<StmSuplier> StmSupliers { get; set; }
        public DbSet<StmProduct> StmProducts { get; set; }
        public DbSet<StmProductOutputDetail> StmProductOutputDetails { get; set; }
        public DbSet<StmProductInputDetail> StmProductInputDetails { get; set; }
        public DbSet<StmCategory> StmCategories { get; set; }
        public DbSet<StmCategoryAttribute> StmCategoryAttributes { get; set; }
        public DbSet<StmProductAttributeValue> StmProductAttributeValues { get; set; }
        public DbSet<StmProductOuput> StmProductOuputs { get; set; }
        public DbSet<StmProductInput> StmProductInputs { get; set; }
        public DbSet<StmAttribute> StmAttributes { get; set; }
        public DbSet<StmAttributeValue> StmAttributeValues { get; set; }
        public DbSet<StmServiceType> StmServiceTypes { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region STM_Account
            modelBuilder.Entity<StmAccount>(cfg =>
            {
                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);
            });
            #endregion

            #region STM_Client
            modelBuilder.Entity<StmClient>(cfg =>
            {
                cfg.Property(f => f.ClientId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(f => f.IsVip)
                    .HasDefaultValue(false);

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);

                cfg.HasMany(f => f.StmProductOutputs)
                    .WithOne(f => f.StmClient)
                    .HasForeignKey(f => f.ClientId)
                    .HasPrincipalKey(f => f.ClientId);
            });
            #endregion

            #region STM_Suplier
            modelBuilder.Entity<StmSuplier>(cfg =>
            {
                cfg.Property(f => f.SuplierId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);

                cfg.HasMany(f => f.StmProductInputs)
                    .WithOne(f => f.StmSuplier)
                    .HasForeignKey(f => f.SuplierId)
                    .HasPrincipalKey(f => f.SuplierId);
            });
            #endregion

            #region STM_Product
            modelBuilder.Entity<StmProduct>(cfg =>
            {
                cfg.Property(f => f.ProductId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(f => f.StmProductAttributeValues)
                    .WithOne(f => f.StmProductNav)
                    .HasForeignKey(f => f.ProductId)
                    .HasPrincipalKey(f => f.ProductId);

                cfg.HasMany(f => f.StmProductOutputDetails)
                    .WithOne(f => f.StmProduct)
                    .HasForeignKey(f => f.ProductId)
                    .HasPrincipalKey(f => f.ProductId);

                cfg.HasMany(f => f.StmProductInputDetails) 
                    .WithOne(f => f.StmProduct)
                    .HasForeignKey(f => f.ProductId)
                    .HasPrincipalKey(f => f.ProductId);

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);
            });
            #endregion

            #region STM_ProductOutputDetails
            modelBuilder.Entity<StmProductOutputDetail>(cfg =>
            {
                cfg.Property(f => f.ProdOutputDetailId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);
            });
            #endregion

            #region STM_ProductInputDetails
            modelBuilder.Entity<StmProductInputDetail>(cfg =>
            {
                cfg.Property(f => f.ProdInputDetailId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);
            });
            #endregion

            #region STM_Category
            modelBuilder.Entity<StmCategory>(cfg =>
            {
                cfg.Property(f => f.CategoryId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(f => f.StmProducts)
                    .WithOne(f => f.CategoryIdNavigation)
                    .HasForeignKey(f => f.CategoryId)
                    .HasPrincipalKey(f => f.CategoryId);

                cfg.HasMany(f => f.StmCategoryAttributes)
                    .WithOne(f => f.StmCategory)
                    .HasForeignKey(f => f.CategoryId)
                    .HasPrincipalKey(f => f.CategoryId);

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);
            });
            #endregion

            #region STM_CategoryAttribute
            modelBuilder.Entity<StmCategoryAttribute>(cfg =>
            {
                cfg.Property(f => f.CategoryAttributeId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasKey(f => new { f.CategoryId, f.AttributeId });

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);
            });
            #endregion

            #region STM_ProductAttributeValue
            modelBuilder.Entity<StmProductAttributeValue>(cfg =>
            {
                cfg.HasKey(f => new { f.ProductId, f.AttributeValueId });

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);
            });
            #endregion

            #region STM_ProductOuput
            modelBuilder.Entity<StmProductOuput>(cfg =>
            {
                cfg.Property(f => f.ProdOuputId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);

                cfg.HasMany(f => f.StmProductOutputDetails)
                    .WithOne(f => f.StmProductOuput)
                    .HasForeignKey(f => f.ProdOutputDetailId)
                    .HasPrincipalKey(f => f.ProdOuputId);

            });
            #endregion

            #region STM_ProductInput
            modelBuilder.Entity<StmProductInput>(cfg =>
            {
                cfg.Property(f => f.ProdInputId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);

                cfg.HasMany(f => f.StmProductInputDetails)
                    .WithOne(f => f.StmProductInput)
                    .HasForeignKey(f => f.ProdInputId)
                    .HasPrincipalKey(f => f.ProdInputId);
            });
            #endregion

            #region STM_ProductOutput
            modelBuilder.Entity<StmProductOuput>(cfg =>
            {
                cfg.Property(f => f.ProdOuputId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);

                cfg.HasMany(f => f.StmProductOutputDetails)
                    .WithOne(f => f.StmProductOuput)
                    .HasForeignKey(f => f.ProdOutputId)
                    .HasPrincipalKey(f => f.ProdOuputId);

                cfg.HasOne(f => f.StmServiceType)
                    .WithMany(f => f.StmProductOuput)
                    .HasPrincipalKey(f => f.TypeId)
                    .HasForeignKey(f => f.ServiceTypeId);
            });
            #endregion

            #region STM_Attribute
            modelBuilder.Entity<StmAttribute>(cfg =>
            {
                cfg.Property(f => f.AttributeId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);

                cfg.HasMany(f => f.StmCategoryAttributes)
                    .WithOne(f => f.StmAttribute)
                    .HasForeignKey(f => f.AttributeId)
                    .HasPrincipalKey(f => f.AttributeId);

                cfg.HasMany(f => f.StmAttributeValues)
                    .WithOne(f => f.StmAttribute)
                    .HasForeignKey(f => f.AttributeId)
                    .HasPrincipalKey(f => f.AttributeId);
            });
            #endregion

            #region STM_AttributeValue
            modelBuilder.Entity<StmAttributeValue>(cfg =>
            {
                cfg.Property(f => f.AttributeValueId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(f => f.StmProductAttributeValues)
                    .WithOne(f => f.StmAttributeValueNav)
                    .HasForeignKey(f => f.AttributeValueId)
                    .HasPrincipalKey(f => f.AttributeValueId);

                cfg.Property(f => f.IsDelete)
                    .HasDefaultValue(false);
            });
            #endregion

            base.OnModelCreating(modelBuilder);
        }

        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var modified = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Added || e.State == EntityState.Deleted);
            foreach (EntityEntry item in modified)
            {
                var changedOrAddedItem = item.Entity as IDateTracking;
                if (changedOrAddedItem != null)
                {
                    if (item.State == EntityState.Added)
                    {
                        changedOrAddedItem.CreateDate = DateTime.UtcNow;
                        changedOrAddedItem.CreateUser = "Script";
                    }
                    changedOrAddedItem.UpdateDate = DateTime.UtcNow;
                    changedOrAddedItem.UpdateUser = "Script";
                }
            }
            return await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        public override int SaveChanges()
        {
            var modified = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Added || e.State == EntityState.Deleted);
            foreach (EntityEntry item in modified)
            {
                var changedOrAddedItem = item.Entity as IDateTracking;
                if (changedOrAddedItem != null)
                {
                    if (item.State == EntityState.Added)
                    {
                        changedOrAddedItem.CreateDate = DateTime.UtcNow;
                        changedOrAddedItem.CreateUser = "Script";
                    }
                    changedOrAddedItem.UpdateDate = DateTime.UtcNow;
                    changedOrAddedItem.UpdateUser = "Script";
                }
            }
            return base.SaveChanges();
        }
    }
}

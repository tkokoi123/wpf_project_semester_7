﻿using StoreManagement.EF.Repositories;
using StoreManagement.EF.Repositories.Interface;

namespace StoreManagement.EF
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppDbContext _context;

        private IStmClientRepository _stmClientRepository;
        private IStmAccountRepository _stmAccountRepository;
        private IStmAttributeRepository _stmAttributeRepository;
        private IStmCategoryAttributeRepository _stmCategoryAttributeRepository;
        private IStmCategoryRepository _stmCategoryRepository;
        private IStmProductRepository _stmProductRepository;
        private IStmSuplierRepository _stmSuplierRepository;
        private IStmAttributeValueRepository _stmAttributeValueRepository;
        private IStmProductAttributeValueRepository _stmProductAttributeValueRepository;
        private IStmProductInputRepository _stmProductInputRepository;
        private IStmProductInputDetailsRepository _stmProductInputDetailsRepository;
        private IStmServiceTypeRepository _stmServiceTypeRepository;
        private IStmProductOutputRepository _stmProductOutputRepository;
        private IStmProductOutputDetailsRepository _stmProductOutputDetailsRepository;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }
        #region ClientRepository
        public IStmClientRepository ClientRepository
        {
            get
            {
                if (_stmClientRepository == null)
                {
                    _stmClientRepository = new StmClientRepository(_context);
                }
                return _stmClientRepository;
            }
        }
        #endregion
        #region AttributeRepository
        public IStmAttributeRepository AttributeRepository
        {
            get
            {
                if (_stmAttributeRepository == null)
                {
                    _stmAttributeRepository = new StmAttributeRepository(_context);
                }
                return _stmAttributeRepository;
            }
        }
        #endregion
        #region CategoryAttributeRepository
        public IStmCategoryAttributeRepository CategoryAttributeRepository
        {
            get
            {
                if (_stmCategoryAttributeRepository == null)
                {
                    _stmCategoryAttributeRepository = new StmCategoryAttributeRepository(_context);
                }
                return _stmCategoryAttributeRepository;
            }
        }
        #endregion
        #region CategoryRepository
        public IStmCategoryRepository CategoryRepository {

            get
            {
                if (_stmCategoryRepository == null)
                {
                    _stmCategoryRepository = new StmCategoryRepository(_context);
                }
                return _stmCategoryRepository;
            }
        }
        #endregion
        #region AccountRepository
        public IStmAccountRepository AccountRepository
        {
            get
            {
                if (_stmAccountRepository == null)
                {
                    _stmAccountRepository = new StmAccountRepository(_context);
                }
                return _stmAccountRepository;
            }
        }
        #endregion
        #region ProductRepository
        public IStmProductRepository ProductRepository
        {

            get
            {
                if (_stmProductRepository == null)
                {
                    _stmProductRepository = new StmProductRepository(_context);
                }
                return _stmProductRepository;
            }
        }
        #endregion
        #region SuplierRepository
        public IStmSuplierRepository SuplierRepository
        {
            get
            {
                if (_stmSuplierRepository == null)
                {
                    _stmSuplierRepository = new StmSuplierRepository(_context);
                }
                return _stmSuplierRepository;
            }
        }
        #endregion
        #region AttributeValueRepository
        public IStmAttributeValueRepository AttributeValueRepository
        {
            get
            {
                if (_stmAttributeValueRepository == null)
                {
                    _stmAttributeValueRepository = new StmAttributeValueRepository(_context);
                }
                return _stmAttributeValueRepository;
            }
        }
        #endregion
        #region ProductAttributeValueRepository
        public IStmProductAttributeValueRepository ProductAttributeValueRepository
        {
            get
            {
                if (_stmProductAttributeValueRepository == null)
                {
                    _stmProductAttributeValueRepository = new StmProductAttributeValueRepository(_context);
                }
                return _stmProductAttributeValueRepository;
            }
        }
        #endregion
        #region ProductInputRepository
        public IStmProductInputRepository ProductInputRepository
        {
            get
            {
                if (_stmProductInputRepository == null)
                {
                    _stmProductInputRepository = new StmProductInputRepository(_context);
                }
                return _stmProductInputRepository;
            }
        }
        #endregion
        #region ProductInputDetailsRepository
        public IStmProductInputDetailsRepository ProductInputDetailsRepository
        {
            get
            {
                if (_stmProductInputDetailsRepository == null)
                {
                    _stmProductInputDetailsRepository = new StmProductInputDetailsRepository(_context);
                }
                return _stmProductInputDetailsRepository;
            }
        }
        #endregion
        #region ProductOutputRepository
        public IStmProductOutputRepository ProductOutputRepository
        {
            get
            {
                if (_stmProductOutputRepository == null)
                {
                    _stmProductOutputRepository = new StmProductOutputRepository(_context);
                }
                return _stmProductOutputRepository;
            }
        }
        #endregion
        #region ProductOutputDetailsRepository
        public IStmProductOutputDetailsRepository ProductOutputDetailsRepository
        {
            get
            {
                if (_stmProductOutputDetailsRepository == null)
                {
                    _stmProductOutputDetailsRepository = new StmProductOutputDetailsRepository(_context);
                }
                return _stmProductOutputDetailsRepository;
            }
        }
        #endregion
        #region ServiceTypeRepository
        public IStmServiceTypeRepository ServiceTypeRepository
        {
            get
            {
                if (_stmServiceTypeRepository == null)
                {
                    _stmServiceTypeRepository = new StmServiceTypeRepository(_context);
                }
                return _stmServiceTypeRepository;
            }
        }
        #endregion
        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task SubmitAsync()
        {
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public void Submit()
        {
            _context.SaveChanges();
        }
    }
}

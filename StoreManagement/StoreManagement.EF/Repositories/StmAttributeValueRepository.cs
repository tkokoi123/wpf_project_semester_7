﻿using Domain.Models;
using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;

namespace StoreManagement.EF.Repositories
{
    public class StmAttributeValueRepository : RepositoryBase<StmAttributeValue>, IStmAttributeValueRepository
    {
        private readonly AppDbContext _context;

        public StmAttributeValueRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public void AddAttributeValues(List<StmAttributeValue> stmAttributeValues)
        {
            _context.AddRange(stmAttributeValues);
            _context.SaveChanges();
        }
    }
}

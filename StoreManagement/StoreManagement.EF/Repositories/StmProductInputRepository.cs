﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.EF.Repositories
{
    public class StmProductInputRepository : RepositoryBase<StmProductInput>, IStmProductInputRepository
    {
        public StmProductInputRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public void AddProductInput(StmProductInput stmProductInput)
        {
            _context.Set<StmProductInput>().Add(stmProductInput);
        }
    }
}

﻿using Domain.Models;
using StoreManagement.EF.Repositories.Interface;

namespace StoreManagement.EF.Repositories
{
    public class StmAccountRepository : RepositoryBase<StmAccount>, IStmAccountRepository
    {
        private readonly AppDbContext _context;

        public StmAccountRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public void AddNewAccount(StmAccount stmAccount)
        {
            _context.Set<StmAccount>().Add(stmAccount);
        }

        public IQueryable<StmAccount> GetAccountByUserName(string username)
        {
            return _context
                    .Set<StmAccount>()
                    .Where(x => x.UserName.Equals(username) && !x.IsDelete);
        }
    }
}

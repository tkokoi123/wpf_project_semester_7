﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.EF.Repositories
{
    public class StmProductInputDetailsRepository : RepositoryBase<StmProductInputDetail>, IStmProductInputDetailsRepository
    {
        public StmProductInputDetailsRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public void AddProductInputDetails(StmProductInputDetail stmProductInputDetail)
        {
            _context.Add(stmProductInputDetail);
        }

    }
}

﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Configurations;

namespace StoreManagement.EF.Repositories
{
    public class StmClientRepository : RepositoryBase<StmClient>,IStmClientRepository
    {
        private readonly AppDbContext _context;

        public StmClientRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<StmClient> GetAllClient()
        {
            return _context.Set<StmClient>()
                    .OrderBy(x => x.DisplayName);
        }

        public IQueryable<StmClient> SearchClient(string displayName)
        {
            return _context.Set<StmClient>()
                    .Where(x => x.DisplayName.ToLower().Contains(displayName.ToLower()));
        }

        public IQueryable<StmClient> GetLimitClient(int index)
        {
            return _context.Set<StmClient>()
                    .Skip(index)
                    .Take(AppSettings.Pagination);
        }

        public void AddNewClient(StmClient stmClient)
        {
            _context.Set<StmClient>()
                    .Add(stmClient);
        }

        public void UpdateClient(StmClient stmClient)
        {
            _context.Set<StmClient>()
                    .Update(stmClient);
        }

        public StmClient GetClientById(Guid id)
        {
            return _context.Set<StmClient>().FirstOrDefault(x => x.ClientId == id && !x.IsDelete);
        }
    }
}

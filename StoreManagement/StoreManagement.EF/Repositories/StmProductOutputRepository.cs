﻿using Domain.Models;
using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.EF.Repositories
{
    public class StmProductOutputRepository : RepositoryBase<StmProductOuput>, IStmProductOutputRepository
    {
        public StmProductOutputRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public void AddProductOutput(StmProductOuput stmProductOuput)
        {
            _context.Set<StmProductOuput>().Add(stmProductOuput);
        }
    }
}

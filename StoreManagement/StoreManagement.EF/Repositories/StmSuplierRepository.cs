﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;

namespace StoreManagement.EF.Repositories
{
    public class StmSuplierRepository : RepositoryBase<StmSuplier>, IStmSuplierRepository
    {
        public StmSuplierRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<StmSuplier> GetSupliers()
        {
            return _context.Set<StmSuplier>();
        }

        public void AddSuplier(StmSuplier stmSuplier)
        {
            _context.Set<StmSuplier>().Add(stmSuplier);
        }

        public void UpdateSuplier(StmSuplier stmSuplier)
        {
            _context.Set<StmSuplier>().Update(stmSuplier);
        }

        public StmSuplier GetSuplierById(Guid id)
        {
            return _context.Set<StmSuplier>().FirstOrDefault(x => x.SuplierId == id && !x.IsDelete);
        }

        public IQueryable<StmSuplier> SearchSuplier(string suplierName)
        {
            return _context.Set<StmSuplier>().Where(x => x.DisplayName.ToLower().Contains(suplierName.ToLower()) && !x.IsDelete);
        }
    }
}

﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.EF.Repositories
{
    public class StmCategoryRepository : RepositoryBase<StmCategory>, IStmCategoryRepository
    {
        public StmCategoryRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public void AddCategory(StmCategory stmCategory)
        {
            _context.Set<StmCategory>().Add(stmCategory);
            _context.SaveChanges();
        }

        public void DisableCategory(StmCategory stmCategory)
        {
            throw new NotImplementedException();
        }

        public StmCategory GetCategoryById(string categoryId)
        {
            return _context.Set<StmCategory>().FirstOrDefault(c => c.CategoryId.Equals(categoryId)
                                && c.IsDelete.Equals(CommonConstant.IsNotDelete));
        }

        public IQueryable<StmCategory> GetCategorys()
        {
            return _context.Set<StmCategory>().Where(a => a.IsDelete.Equals(CommonConstant.IsNotDelete));
        }

        public IQueryable<StmCategory> SearchCategoryByName(string categoryName)
        {
            return _context.Set<StmCategory>().Where(c => c.CategoryName.ToLower().Contains(categoryName.ToLower()))
                                              .Where(a => a.IsDelete.Equals(CommonConstant.IsNotDelete));
        }

        public void UpdateCategory(StmCategory stmCategory)
        {
            _context.Set<StmCategory>().Update(stmCategory);
            _context.SaveChanges();
        }
        
    }
}

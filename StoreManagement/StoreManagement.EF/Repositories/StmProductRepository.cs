﻿using Microsoft.EntityFrameworkCore;
using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;

namespace StoreManagement.EF.Repositories
{
    public class StmProductRepository : RepositoryBase<StmProduct>, IStmProductRepository
    {
        public StmProductRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public IQueryable<StmProduct> GetProductByCategory(Guid categoryId)
        {
            return _context.Set<StmProduct>().Where(x => x.CategoryId.Equals(categoryId));
        }
        public bool CategoryExistInProduct(Guid categoryId)
        {
            var resutl = _context.Set<StmProduct>().FirstOrDefault(p => p.CategoryId.Equals(categoryId));
            return resutl != null;
        }
        public void AddProduct(StmProduct stmProduct)
        {
            _context.Set<StmProduct>().Add(stmProduct);
        }
        //get product by id and include productAttribute , attributeValue, attribute 
        public StmProduct GetProductById(Guid productId)
        {
            return _context.Set<StmProduct>().Include(p => p.StmProductAttributeValues)
                                                .ThenInclude(pav => pav.StmAttributeValueNav)
                                                    .ThenInclude(av => av.StmAttribute)
                .FirstOrDefault(p => p.ProductId.Equals(productId));
        }
        public void UpdateProduct(StmProduct stmProduct)
        {
            _context.Set<StmProduct>().Update(stmProduct);
        }

        public IQueryable<StmProduct> GetAll()
        {
            return _context.Set<StmProduct>().Include(p => p.StmProductAttributeValues)
                                             .ThenInclude(pav => pav.StmAttributeValueNav)
                                             .ThenInclude(av => av.StmAttribute);
        }
    }
}

﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.EF.Repositories
{
    public class StmAttributeRepository : RepositoryBase<StmAttribute>, IStmAttributeRepository {
        public StmAttributeRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        //Get all attributes in system
        public IQueryable<StmAttribute> GetAttributes()
        {
            return _context.Set<StmAttribute>().Where(a=>a.IsDelete.Equals(CommonConstant.IsNotDelete));
        }
        
        public void AddAttribute(StmAttribute stmAttribute)
        {
                _context.Set<StmAttribute>().Add(stmAttribute);
                _context.SaveChanges();
        }
        public void UpdateAttribute(StmAttribute stmAttribute)
        {
                _context.Set<StmAttribute>().Update(stmAttribute);
                _context.SaveChanges();
        }
        public void DisableAttribute(StmAttribute stmAttribute)
        {
                stmAttribute.IsDelete = CommonConstant.IsDelete;
                _context.Set<StmAttribute>().Update(stmAttribute);
                _context.SaveChanges();
        }
        public StmAttribute GetAttributeById(string attributeId) {
            return _context.Set<StmAttribute>().FirstOrDefault(a => a.AttributeId.Equals(attributeId));
        }

        public IQueryable<StmAttribute> SearchAttributeByName(string attributeName)
        {
            return _context.Set<StmAttribute>().Where(a => a.AttributeName.ToLower().Contains(attributeName.ToLower()))
                                               .Where(a => a.IsDelete.Equals(CommonConstant.IsNotDelete));
        }
        //Get IQueryable attribute by Category Id in entity stmCategoryAttribute
        public IQueryable<StmAttribute> GetAttributesByCategoryId(Guid categoryId)
        {
            var result = from ca in _context.Set<StmCategoryAttribute>()
                         join a in _context.Set<StmAttribute>() on ca.AttributeId equals a.AttributeId
                         where ca.CategoryId.Equals(categoryId)
                         && a.IsDelete.Equals(CommonConstant.IsNotDelete)
                         && ca.IsDelete.Equals(CommonConstant.IsNotDelete)
                         select a;
            return result;
        }
        public IQueryable<StmAttribute> GetAttributesNotContaintListAttributeId(List<string> attributeIds)
        {
            return _context.Set<StmAttribute>().Where(a=>!attributeIds.Contains(a.AttributeId.ToString()))
                                               .Where(a=>a.IsDelete.Equals(CommonConstant.IsNotDelete));
        }
        public IQueryable<StmAttribute> GetAttributesByAttributeNameAndNotContaintListAttributeId(string attributeName,List<string> attributeIds)
        {
            return _context.Set<StmAttribute>().Where(a => !attributeIds.Contains(a.AttributeId.ToString()))
                                               .Where(a=>a.AttributeName.ToLower().Contains(attributeName.ToLower()))
                                               .Where(a => a.IsDelete.Equals(CommonConstant.IsNotDelete));
        }
        //Get IQueryable attribute by attributename,(Category Id in entity stmCategoryAttribute)
        public IQueryable<StmAttribute> GetAttributesByCategoryIdAndAttributeName(Guid categoryId, string attributeName)
        {
            try
            {
                var result = from ca in _context.Set<StmCategoryAttribute>()
                             join a in _context.Set<StmAttribute>() on ca.AttributeId equals a.AttributeId
                             where ca.CategoryId.Equals(categoryId)
                             && a.AttributeName.ToLower().Contains(attributeName.ToLower())
                             && a.IsDelete.Equals(CommonConstant.IsNotDelete)
                             && ca.IsDelete.Equals(CommonConstant.IsNotDelete)
                             select a;
                return result;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}

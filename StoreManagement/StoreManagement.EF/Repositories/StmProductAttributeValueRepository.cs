﻿using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.EF.Repositories
{
    public class StmProductAttributeValueRepository : RepositoryBase<StmProductAttributeValue>, IStmProductAttributeValueRepository
    {
        public StmProductAttributeValueRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public void AddStmProductAttributeValue(StmProductAttributeValue stmProductAttributeValue)
        {
            _context.Set<StmProductAttributeValue>().Add(stmProductAttributeValue);
        }
    }
}

﻿using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmAttributeValueRepository 
    {
        public void AddAttributeValues(List<StmAttributeValue> stmAttributeValues);
    }
}

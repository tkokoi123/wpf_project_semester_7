﻿using Domain.Models;
using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmSuplierRepository
    {
        public IQueryable<StmSuplier> GetSupliers();
        public void AddSuplier(StmSuplier stmSuplier);
        public void UpdateSuplier(StmSuplier stmSuplier);
        public StmSuplier GetSuplierById(Guid id);
        public IQueryable<StmSuplier> SearchSuplier(string suplierName);
    }
}

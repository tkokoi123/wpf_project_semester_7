﻿using Domain.Models;
using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmProductOutputRepository
    {
        public void AddProductOutput(StmProductOuput stmProductOuput);
    }
}

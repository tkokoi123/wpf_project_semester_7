﻿namespace StoreManagement.EF.Repositories.Interface
{
    public interface IBaseRepository<TEntity>
    {
        IQueryable<TEntity> GetAll();
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }
}

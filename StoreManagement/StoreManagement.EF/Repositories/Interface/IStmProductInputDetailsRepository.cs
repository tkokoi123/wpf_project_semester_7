﻿using StoreManagement.Domain.Models;
using StoreManagement.Domain.RawModels;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmProductInputDetailsRepository
    {
        public void AddProductInputDetails(StmProductInputDetail stmProductInputDetail);
    }
}

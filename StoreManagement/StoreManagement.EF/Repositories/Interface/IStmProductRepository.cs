﻿using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmProductRepository
    {
        public IQueryable<StmProduct> GetProductByCategory(Guid categoryId);
        public bool CategoryExistInProduct(Guid categoryId);
        public void AddProduct(StmProduct stmProduct);
        public StmProduct GetProductById(Guid productId);
        public void UpdateProduct(StmProduct stmProduct);
        public IQueryable<StmProduct> GetAll();
    }
}

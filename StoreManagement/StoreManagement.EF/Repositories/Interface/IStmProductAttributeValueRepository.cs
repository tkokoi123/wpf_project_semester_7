﻿using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmProductAttributeValueRepository
    {
        public void AddStmProductAttributeValue(StmProductAttributeValue stmProductAttributeValue);
    }
}

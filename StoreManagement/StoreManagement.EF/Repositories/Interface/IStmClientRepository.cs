﻿using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmClientRepository
    {
        IQueryable<StmClient> GetAllClient();
        IQueryable<StmClient> GetLimitClient(int index);
        IQueryable<StmClient> SearchClient(string displayName);
        void AddNewClient(StmClient stmClient);
        void UpdateClient(StmClient stmClient);
        StmClient GetClientById(Guid id);
    }
}

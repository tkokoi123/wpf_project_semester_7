﻿using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmCategoryRepository
    {
        public IQueryable<StmCategory> GetCategorys();
        public void AddCategory(StmCategory stmCategory);
        public void UpdateCategory(StmCategory stmCategory);
        public void DisableCategory(StmCategory stmCategory);
        public StmCategory GetCategoryById(string categoryId);
        public IQueryable<StmCategory> SearchCategoryByName(string categoryName);
    }
}

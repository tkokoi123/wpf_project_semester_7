﻿using Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmAccountRepository
    {
        public IQueryable<StmAccount> GetAccountByUserName(string username);
        public void AddNewAccount(StmAccount stmAccount);
    }
}

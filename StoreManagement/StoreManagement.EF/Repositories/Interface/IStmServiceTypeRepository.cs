﻿using Domain.Models;
using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmServiceTypeRepository
    {
        public IQueryable<StmServiceType> GetServiceTypes();
    }
}

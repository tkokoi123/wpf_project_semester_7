﻿using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmCategoryAttributeRepository
    {
        public bool AttributeExistInCategoryAttribute(StmAttribute stmAttribute);
        public void AddCategoryAttribute(StmCategoryAttribute stmCategoryAttribute);
        public void DeleteCategoryAttribute(StmCategoryAttribute stmCategoryAttribute);
        
        
    }
}

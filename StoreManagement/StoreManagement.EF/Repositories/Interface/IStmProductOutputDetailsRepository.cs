﻿using Domain.Models;
using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmProductOutputDetailsRepository
    {
        public void AddOutputProductDetails(List<StmProductOutputDetail> stmProductOutputDetail);
    }
}

﻿using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmAttributeRepository 
    {
        //Get all attributes in system
        public IQueryable<StmAttribute> GetAttributes();
        public void AddAttribute(StmAttribute stmAttribute);
        public void UpdateAttribute(StmAttribute stmAttribute);
        public void DisableAttribute(StmAttribute stmAttribute);
        public StmAttribute GetAttributeById(string attributeId);
        public IQueryable<StmAttribute> SearchAttributeByName(string attributeName);
        public IQueryable<StmAttribute> GetAttributesByCategoryId(Guid categoryId);
        public IQueryable<StmAttribute> GetAttributesNotContaintListAttributeId(List<string> attributeIds);
        public IQueryable<StmAttribute> GetAttributesByAttributeNameAndNotContaintListAttributeId(string attributeName, List<string> attributeIds);
        public IQueryable<StmAttribute> GetAttributesByCategoryIdAndAttributeName(Guid categoryId, string attributeName);
    }
}

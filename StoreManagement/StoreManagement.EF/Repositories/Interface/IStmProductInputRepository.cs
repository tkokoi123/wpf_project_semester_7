﻿using StoreManagement.Domain.Models;

namespace StoreManagement.EF.Repositories.Interface
{
    public interface IStmProductInputRepository
    {
        public void AddProductInput(StmProductInput stmProductInput);
    }
}

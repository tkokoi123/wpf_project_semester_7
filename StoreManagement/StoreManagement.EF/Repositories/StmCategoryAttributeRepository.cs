﻿using Microsoft.EntityFrameworkCore;
using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.EF.Repositories
{
    public class StmCategoryAttributeRepository : RepositoryBase<StmCategoryAttribute>, IStmCategoryAttributeRepository
    {
        public StmCategoryAttributeRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        //Check attribute exist in entity categoryAttribute
        public bool AttributeExistInCategoryAttribute(StmAttribute stmAttribute) {
            var categoryAttribute = _context.Set<StmCategoryAttribute>().
                FirstOrDefault(ca => ca.AttributeId.Equals(stmAttribute.AttributeId));
            if (categoryAttribute == null) {
                return false;
            }
            return true;
        }
        
        public void AddCategoryAttribute(StmCategoryAttribute stmCategoryAttribute)
        {
            var newStmCategoryAttribute = GetStmCategoryAttribute(stmCategoryAttribute.AttributeId, stmCategoryAttribute.CategoryId);
            if (newStmCategoryAttribute != null) {
                newStmCategoryAttribute.IsDelete = CommonConstant.IsNotDelete;
                _context.Update(newStmCategoryAttribute);
            }
            else
            {
                _context.Add(stmCategoryAttribute);
            }
            _context.SaveChanges();
        }
        public void DeleteCategoryAttribute(StmCategoryAttribute stmCategoryAttribute)
        {
            var newStmCategoryAttribute = GetStmCategoryAttribute(stmCategoryAttribute.AttributeId, stmCategoryAttribute.CategoryId);
            newStmCategoryAttribute.IsDelete = CommonConstant.IsDelete;
            _context.Update(newStmCategoryAttribute);
            _context.SaveChanges();
        }
        public StmCategoryAttribute GetStmCategoryAttribute(Guid attributeId, Guid categoryId) {
            return _context.Set<StmCategoryAttribute>().FirstOrDefault(ca => ca.AttributeId.Equals(attributeId)
                                                     && ca.CategoryId.Equals(categoryId));
        }
        
    }
}

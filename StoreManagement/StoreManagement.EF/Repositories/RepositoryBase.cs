﻿using StoreManagement.EF.Repositories.Interface;

namespace StoreManagement.EF.Repositories
{
    public abstract class RepositoryBase<T> : IBaseRepository<T> 
        where T : class
    {
        protected AppDbContext _context;

        protected RepositoryBase(AppDbContext context)
        {
            _context = context;
        }

        public void Create(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public void Update(T entity)
        {
            _context.Set<T>().Update(entity);
        }
    }
}

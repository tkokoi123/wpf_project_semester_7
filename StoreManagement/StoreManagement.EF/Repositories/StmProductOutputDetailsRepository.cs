﻿using Domain.Models;
using StoreManagement.Domain.Models;
using StoreManagement.EF.Repositories.Interface;
using StoreManagement.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreManagement.EF.Repositories
{
    public class StmProductOutputDetailsRepository : RepositoryBase<StmProductOutputDetail>, IStmProductOutputDetailsRepository
    {
        public StmProductOutputDetailsRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public void AddOutputProductDetails(List<StmProductOutputDetail> stmProductOutputDetail)
        {
            _context.Set<StmProductOutputDetail>().AddRange(stmProductOutputDetail);    
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StoreManagement.EF.Migrations
{
    public partial class InitDB_v8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AttributeValueName",
                table: "STM_AttributeValue",
                newName: "AttributeValue");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AttributeValue",
                table: "STM_AttributeValue",
                newName: "AttributeValueName");
        }
    }
}

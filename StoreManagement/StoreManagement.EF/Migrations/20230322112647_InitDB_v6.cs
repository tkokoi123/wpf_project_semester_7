﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace StoreManagement.EF.Migrations
{
    public partial class InitDB_v6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductInputDetails_STM_Suplier_SuplierId",
                table: "STM_ProductInputDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductOutputDetails_STM_Client_ClientId",
                table: "STM_ProductOutputDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductOutputDetails_STM_ProductOuput_ProdOutputDetailId",
                table: "STM_ProductOutputDetails");

            migrationBuilder.DropIndex(
                name: "IX_STM_ProductOutputDetails_ClientId",
                table: "STM_ProductOutputDetails");

            migrationBuilder.DropIndex(
                name: "IX_STM_ProductInputDetails_SuplierId",
                table: "STM_ProductInputDetails");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "STM_ProductOutputDetails");

            migrationBuilder.DropColumn(
                name: "SuplierId",
                table: "STM_ProductInputDetails");

            migrationBuilder.AddColumn<Guid>(
                name: "ClientId",
                table: "STM_ProductOuput",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "STM_ProductOuput",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<float>(
                name: "Discount",
                table: "STM_ProductOuput",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "PrePayment",
                table: "STM_ProductOuput",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "ServiceFee",
                table: "STM_ProductOuput",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<int>(
                name: "ServiceTypeId",
                table: "STM_ProductOuput",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "STM_ProductInput",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<float>(
                name: "PreMoney",
                table: "STM_ProductInput",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<Guid>(
                name: "SuplierId",
                table: "STM_ProductInput",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "StmServiceType",
                columns: table => new
                {
                    TypeId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TypeName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StmServiceType", x => x.TypeId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductOutputDetails_ProdOutputId",
                table: "STM_ProductOutputDetails",
                column: "ProdOutputId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductOuput_ClientId",
                table: "STM_ProductOuput",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductOuput_ServiceTypeId",
                table: "STM_ProductOuput",
                column: "ServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductInput_SuplierId",
                table: "STM_ProductInput",
                column: "SuplierId");

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductInput_STM_Suplier_SuplierId",
                table: "STM_ProductInput",
                column: "SuplierId",
                principalTable: "STM_Suplier",
                principalColumn: "SuplierId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductOuput_STM_Client_ClientId",
                table: "STM_ProductOuput",
                column: "ClientId",
                principalTable: "STM_Client",
                principalColumn: "ClientId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductOuput_StmServiceType_ServiceTypeId",
                table: "STM_ProductOuput",
                column: "ServiceTypeId",
                principalTable: "StmServiceType",
                principalColumn: "TypeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductOutputDetails_STM_ProductOuput_ProdOutputId",
                table: "STM_ProductOutputDetails",
                column: "ProdOutputId",
                principalTable: "STM_ProductOuput",
                principalColumn: "ProdOuputId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductInput_STM_Suplier_SuplierId",
                table: "STM_ProductInput");

            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductOuput_STM_Client_ClientId",
                table: "STM_ProductOuput");

            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductOuput_StmServiceType_ServiceTypeId",
                table: "STM_ProductOuput");

            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductOutputDetails_STM_ProductOuput_ProdOutputId",
                table: "STM_ProductOutputDetails");

            migrationBuilder.DropTable(
                name: "StmServiceType");

            migrationBuilder.DropIndex(
                name: "IX_STM_ProductOutputDetails_ProdOutputId",
                table: "STM_ProductOutputDetails");

            migrationBuilder.DropIndex(
                name: "IX_STM_ProductOuput_ClientId",
                table: "STM_ProductOuput");

            migrationBuilder.DropIndex(
                name: "IX_STM_ProductOuput_ServiceTypeId",
                table: "STM_ProductOuput");

            migrationBuilder.DropIndex(
                name: "IX_STM_ProductInput_SuplierId",
                table: "STM_ProductInput");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "STM_ProductOuput");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "STM_ProductOuput");

            migrationBuilder.DropColumn(
                name: "Discount",
                table: "STM_ProductOuput");

            migrationBuilder.DropColumn(
                name: "PrePayment",
                table: "STM_ProductOuput");

            migrationBuilder.DropColumn(
                name: "ServiceFee",
                table: "STM_ProductOuput");

            migrationBuilder.DropColumn(
                name: "ServiceTypeId",
                table: "STM_ProductOuput");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "STM_ProductInput");

            migrationBuilder.DropColumn(
                name: "PreMoney",
                table: "STM_ProductInput");

            migrationBuilder.DropColumn(
                name: "SuplierId",
                table: "STM_ProductInput");

            migrationBuilder.AddColumn<Guid>(
                name: "ClientId",
                table: "STM_ProductOutputDetails",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "SuplierId",
                table: "STM_ProductInputDetails",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductOutputDetails_ClientId",
                table: "STM_ProductOutputDetails",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductInputDetails_SuplierId",
                table: "STM_ProductInputDetails",
                column: "SuplierId");

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductInputDetails_STM_Suplier_SuplierId",
                table: "STM_ProductInputDetails",
                column: "SuplierId",
                principalTable: "STM_Suplier",
                principalColumn: "SuplierId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductOutputDetails_STM_Client_ClientId",
                table: "STM_ProductOutputDetails",
                column: "ClientId",
                principalTable: "STM_Client",
                principalColumn: "ClientId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductOutputDetails_STM_ProductOuput_ProdOutputDetailId",
                table: "STM_ProductOutputDetails",
                column: "ProdOutputDetailId",
                principalTable: "STM_ProductOuput",
                principalColumn: "ProdOuputId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

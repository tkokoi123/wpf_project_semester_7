﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StoreManagement.EF.Migrations
{
    public partial class InitDB_v7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductInputDetails_STM_ProductInput_ProdInputDetailId",
                table: "STM_ProductInputDetails");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductInputDetails_ProdInputId",
                table: "STM_ProductInputDetails",
                column: "ProdInputId");

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductInputDetails_STM_ProductInput_ProdInputId",
                table: "STM_ProductInputDetails",
                column: "ProdInputId",
                principalTable: "STM_ProductInput",
                principalColumn: "ProdInputId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_STM_ProductInputDetails_STM_ProductInput_ProdInputId",
                table: "STM_ProductInputDetails");

            migrationBuilder.DropIndex(
                name: "IX_STM_ProductInputDetails_ProdInputId",
                table: "STM_ProductInputDetails");

            migrationBuilder.AddForeignKey(
                name: "FK_STM_ProductInputDetails_STM_ProductInput_ProdInputDetailId",
                table: "STM_ProductInputDetails",
                column: "ProdInputDetailId",
                principalTable: "STM_ProductInput",
                principalColumn: "ProdInputId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

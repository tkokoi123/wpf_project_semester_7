﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StoreManagement.EF.Migrations
{
    public partial class InitDB_V1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "STM_Account",
                columns: table => new
                {
                    UserName = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    Password = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_Account", x => x.UserName);
                });

            migrationBuilder.CreateTable(
                name: "STM_AttributeValue",
                columns: table => new
                {
                    AttributeValueId = table.Column<Guid>(type: "uuid", nullable: false),
                    AttributeId = table.Column<Guid>(type: "uuid", nullable: false),
                    AttributeValueName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    AttributeValueDescription = table.Column<string>(type: "text", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_AttributeValue", x => x.AttributeValueId);
                });

            migrationBuilder.CreateTable(
                name: "STM_Category",
                columns: table => new
                {
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    CategoryName = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_Category", x => x.CategoryId);
                });

            migrationBuilder.CreateTable(
                name: "STM_Client",
                columns: table => new
                {
                    ClientId = table.Column<Guid>(type: "uuid", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    Address = table.Column<string>(type: "text", nullable: false),
                    Phone = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    Email = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    MoreInfo = table.Column<string>(type: "text", nullable: false),
                    IsVip = table.Column<bool>(type: "boolean", nullable: false, defaultValue: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_Client", x => x.ClientId);
                });

            migrationBuilder.CreateTable(
                name: "STM_Suplier",
                columns: table => new
                {
                    SuplierId = table.Column<Guid>(type: "uuid", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    Address = table.Column<string>(type: "text", nullable: false),
                    Phone = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    Email = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    MoreInfo = table.Column<string>(type: "text", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_Suplier", x => x.SuplierId);
                });

            migrationBuilder.CreateTable(
                name: "STM_CategoryAttribute",
                columns: table => new
                {
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    AttributeId = table.Column<Guid>(type: "uuid", nullable: false),
                    CategoryAttributeId = table.Column<Guid>(type: "uuid", nullable: false),
                    StmCategoryCategoryId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_CategoryAttribute", x => new { x.CategoryId, x.AttributeId });
                    table.ForeignKey(
                        name: "FK_STM_CategoryAttribute_STM_Category_StmCategoryCategoryId",
                        column: x => x.StmCategoryCategoryId,
                        principalTable: "STM_Category",
                        principalColumn: "CategoryId");
                });

            migrationBuilder.CreateTable(
                name: "STM_Product",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductName = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false),
                    ProductDescription = table.Column<string>(type: "text", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_Product", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_STM_Product_STM_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "STM_Category",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "STM_ProductAttributeValue",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    AttributeValueId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_ProductAttributeValue", x => new { x.ProductId, x.AttributeValueId });
                    table.ForeignKey(
                        name: "FK_STM_ProductAttributeValue_STM_AttributeValue_AttributeValue~",
                        column: x => x.AttributeValueId,
                        principalTable: "STM_AttributeValue",
                        principalColumn: "AttributeValueId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_STM_ProductAttributeValue_STM_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "STM_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_STM_CategoryAttribute_StmCategoryCategoryId",
                table: "STM_CategoryAttribute",
                column: "StmCategoryCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_Product_CategoryId",
                table: "STM_Product",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductAttributeValue_AttributeValueId",
                table: "STM_ProductAttributeValue",
                column: "AttributeValueId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "STM_Account");

            migrationBuilder.DropTable(
                name: "STM_CategoryAttribute");

            migrationBuilder.DropTable(
                name: "STM_Client");

            migrationBuilder.DropTable(
                name: "STM_ProductAttributeValue");

            migrationBuilder.DropTable(
                name: "STM_Suplier");

            migrationBuilder.DropTable(
                name: "STM_AttributeValue");

            migrationBuilder.DropTable(
                name: "STM_Product");

            migrationBuilder.DropTable(
                name: "STM_Category");
        }
    }
}

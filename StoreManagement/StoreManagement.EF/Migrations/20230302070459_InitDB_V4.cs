﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StoreManagement.EF.Migrations
{
    public partial class InitDB_V4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_STM_CategoryAttribute_STM_Category_StmCategoryCategoryId",
                table: "STM_CategoryAttribute");

            migrationBuilder.DropIndex(
                name: "IX_STM_CategoryAttribute_StmCategoryCategoryId",
                table: "STM_CategoryAttribute");

            migrationBuilder.DropColumn(
                name: "StmCategoryCategoryId",
                table: "STM_CategoryAttribute");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_Suplier",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_ProductAttributeValue",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_Product",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AlterColumn<bool>(
                name: "IsVip",
                table: "STM_Client",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_Client",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_CategoryAttribute",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_Category",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AddColumn<string>(
                name: "CategoryDescription",
                table: "STM_Category",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_AttributeValue",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.CreateTable(
                name: "STM_Attribute",
                columns: table => new
                {
                    AttributeId = table.Column<Guid>(type: "uuid", nullable: false),
                    AttributeName = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: false),
                    AttributeDescription = table.Column<string>(type: "text", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_Attribute", x => x.AttributeId);
                });

            migrationBuilder.CreateTable(
                name: "STM_ProductInput",
                columns: table => new
                {
                    ProdInputId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_ProductInput", x => x.ProdInputId);
                });

            migrationBuilder.CreateTable(
                name: "STM_ProductOuput",
                columns: table => new
                {
                    ProdOuputId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_ProductOuput", x => x.ProdOuputId);
                });

            migrationBuilder.CreateTable(
                name: "STM_ProductInputDetails",
                columns: table => new
                {
                    ProdInputDetailId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProdInputId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    SuplierId = table.Column<Guid>(type: "uuid", nullable: false),
                    Quantity = table.Column<int>(type: "integer", nullable: false),
                    InputPrice = table.Column<float>(type: "real", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_ProductInputDetails", x => x.ProdInputDetailId);
                    table.ForeignKey(
                        name: "FK_STM_ProductInputDetails_STM_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "STM_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_STM_ProductInputDetails_STM_ProductInput_ProdInputDetailId",
                        column: x => x.ProdInputDetailId,
                        principalTable: "STM_ProductInput",
                        principalColumn: "ProdInputId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_STM_ProductInputDetails_STM_Suplier_SuplierId",
                        column: x => x.SuplierId,
                        principalTable: "STM_Suplier",
                        principalColumn: "SuplierId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "STM_ProductOutputDetails",
                columns: table => new
                {
                    ProdOutputDetailId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProdOutputId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClientId = table.Column<Guid>(type: "uuid", nullable: false),
                    Quantity = table.Column<int>(type: "integer", nullable: false),
                    OutputPrice = table.Column<float>(type: "real", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STM_ProductOutputDetails", x => x.ProdOutputDetailId);
                    table.ForeignKey(
                        name: "FK_STM_ProductOutputDetails_STM_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "STM_Client",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_STM_ProductOutputDetails_STM_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "STM_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_STM_ProductOutputDetails_STM_ProductOuput_ProdOutputDetailId",
                        column: x => x.ProdOutputDetailId,
                        principalTable: "STM_ProductOuput",
                        principalColumn: "ProdOuputId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_STM_CategoryAttribute_AttributeId",
                table: "STM_CategoryAttribute",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_AttributeValue_AttributeId",
                table: "STM_AttributeValue",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductInputDetails_ProductId",
                table: "STM_ProductInputDetails",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductInputDetails_SuplierId",
                table: "STM_ProductInputDetails",
                column: "SuplierId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductOutputDetails_ClientId",
                table: "STM_ProductOutputDetails",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_STM_ProductOutputDetails_ProductId",
                table: "STM_ProductOutputDetails",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_STM_AttributeValue_STM_Attribute_AttributeId",
                table: "STM_AttributeValue",
                column: "AttributeId",
                principalTable: "STM_Attribute",
                principalColumn: "AttributeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_STM_CategoryAttribute_STM_Attribute_AttributeId",
                table: "STM_CategoryAttribute",
                column: "AttributeId",
                principalTable: "STM_Attribute",
                principalColumn: "AttributeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_STM_CategoryAttribute_STM_Category_CategoryId",
                table: "STM_CategoryAttribute",
                column: "CategoryId",
                principalTable: "STM_Category",
                principalColumn: "CategoryId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_STM_AttributeValue_STM_Attribute_AttributeId",
                table: "STM_AttributeValue");

            migrationBuilder.DropForeignKey(
                name: "FK_STM_CategoryAttribute_STM_Attribute_AttributeId",
                table: "STM_CategoryAttribute");

            migrationBuilder.DropForeignKey(
                name: "FK_STM_CategoryAttribute_STM_Category_CategoryId",
                table: "STM_CategoryAttribute");

            migrationBuilder.DropTable(
                name: "STM_Attribute");

            migrationBuilder.DropTable(
                name: "STM_ProductInputDetails");

            migrationBuilder.DropTable(
                name: "STM_ProductOutputDetails");

            migrationBuilder.DropTable(
                name: "STM_ProductInput");

            migrationBuilder.DropTable(
                name: "STM_ProductOuput");

            migrationBuilder.DropIndex(
                name: "IX_STM_CategoryAttribute_AttributeId",
                table: "STM_CategoryAttribute");

            migrationBuilder.DropIndex(
                name: "IX_STM_AttributeValue_AttributeId",
                table: "STM_AttributeValue");

            migrationBuilder.DropColumn(
                name: "CategoryDescription",
                table: "STM_Category");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_Suplier",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_ProductAttributeValue",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_Product",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<bool>(
                name: "IsVip",
                table: "STM_Client",
                type: "boolean",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_Client",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_CategoryAttribute",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "StmCategoryCategoryId",
                table: "STM_CategoryAttribute",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_Category",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "STM_AttributeValue",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_STM_CategoryAttribute_StmCategoryCategoryId",
                table: "STM_CategoryAttribute",
                column: "StmCategoryCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_STM_CategoryAttribute_STM_Category_StmCategoryCategoryId",
                table: "STM_CategoryAttribute",
                column: "StmCategoryCategoryId",
                principalTable: "STM_Category",
                principalColumn: "CategoryId");
        }
    }
}

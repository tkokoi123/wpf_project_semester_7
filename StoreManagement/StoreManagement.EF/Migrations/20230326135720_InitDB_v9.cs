﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StoreManagement.EF.Migrations
{
    public partial class InitDB_v9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "EstimatedPrice",
                table: "STM_Product",
                type: "real",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstimatedPrice",
                table: "STM_Product");
        }
    }
}

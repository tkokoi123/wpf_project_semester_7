﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using StoreManagement.Infrastructure.Configurations;
using System.Text;

namespace StoreManagement.Infrastructure.Helpers
{
    public static class HashHelper
    {
        public static string EncryptPassword(string originPassword)
        {
            if (string.IsNullOrEmpty(originPassword))
            {
                return "";
            }
            byte[] saltkey = Encoding.UTF8.GetBytes(AppSettings.Salt);
            string encryptPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: originPassword,
                salt: saltkey,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return encryptPassword;
        }

        public static bool ComparePassword(string dbPassword, string userPassword)
        {
            string encryptedUserPassword = EncryptPassword(userPassword);
            return dbPassword.Equals(encryptedUserPassword);
        }
    }
}

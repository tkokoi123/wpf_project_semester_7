﻿using System.Text;

namespace StoreManagement.Infrastructure.Helpers
{
    public static class StringHelper
    {
        /// <summary>
        ///     Convert string to base64 format
        /// </summary>
        /// <param name="enteredString"></param>
        /// <returns>Base64 format string</returns>
        public static string ConvertStringToBase64(string enteredString)
        {
            var stringBytes = Encoding.UTF8.GetBytes(enteredString);
            return Convert.ToBase64String(stringBytes);
        }

        /// <summary>
        ///     Convert base64 string to original
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns>Original string</returns>
        public static string ConvertBase64ToString(string base64String)
        {
            var stringBytes = Convert.FromBase64String(base64String);
            return Encoding.UTF8.GetString(stringBytes);
        }
    }
}

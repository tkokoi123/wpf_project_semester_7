﻿using System.Reflection;
using System.Resources;

namespace StoreManagement.Infrastructure.Language
{
    public static class LanguageManagement
    {
        public static string GetString(string culture, string msgCode)
        {
            if (string.IsNullOrEmpty(culture))
            {
                culture = "vn";
            }
            string result = string.Empty;
            switch (culture.ToLowerInvariant())
            {
                case "vn":
                    ResourceManager vn = new ResourceManager("Language-Vi", Assembly.GetExecutingAssembly());
                    result = vn.GetString(msgCode)!;
                    break;

                case "en":
                    ResourceManager en = new ResourceManager("Language-En", Assembly.GetExecutingAssembly());
                    result = en.GetString(msgCode)!;
                    break;

                default:
                    ResourceManager df = new ResourceManager("Language-Vi", Assembly.GetExecutingAssembly());
                    result = df.GetString(msgCode)!;
                    break;
            }
            return result;
        }
    }
}

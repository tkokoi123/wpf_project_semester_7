﻿namespace StoreManagement.Infrastructure.Constants
{
    public static class CommonConstant
    {
        public const string MaterialDesignLink = "http://materialdesigninxaml.net/winfx/xaml/themes";
        public const bool IsNotDelete = false;
        public const bool IsDelete = true;
    }
}

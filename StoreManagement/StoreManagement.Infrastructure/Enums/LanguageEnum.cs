﻿namespace StoreManagement.Infrastructure.Enums
{
    public enum Language
    {
        VN = 0,
        EN = 1
    }

    public enum Role
    {
        SA = 0,
        CL = 1
    }
}

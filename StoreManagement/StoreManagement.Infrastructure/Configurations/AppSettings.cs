﻿using Microsoft.Extensions.Configuration;

namespace StoreManagement.Infrastructure.Configurations
{
    public static class AppSettings
    {
        private static IConfiguration Configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

        public static string Salt
        {
            get { return Configuration["Security:Salt"]!; }
        }

        public static string Connection
        {
            get { return Configuration["ConnectionStrings:Default"]!; }
        }

        public static int Pagination
        {
            get { return Convert.ToInt32(Configuration["Pagination:Client"]!); }
        }
    }
}
